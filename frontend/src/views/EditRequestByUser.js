import React, { useEffect, useRef, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { Link, Redirect, useHistory } from "react-router-dom";
import {
  getSingleRequest,
  toastEndpoint,
  setUpdatedVacationRequest,
  postNewComment,
  deleteRequest,
  getIneligiblePeriods,
} from "../api/EndPoints";
import { getStorage } from "../utils/localStorage";
import moment from "moment";
import DatePicker from "react-datepicker";
import { useSelector } from "react-redux";
import { isDateEligible, isDateRangeEligible } from "../utils/helperFunctions";

const EditRequestByUser = (props) => {
  const user = useSelector((state) => state.user);
  const [data, setData] = useState({
    id: -1,
    title: "",
    period_start: "",
    period_end: "",
    status_id: 2,
    tidsbankenUser: {
      id: -1,
      first_name: "",
      last_name: "",
    },
    comments: [
      {
        id: -1,
        message: "",
        tidsbankenUser: {
          first_name: "",
        },
        time_created: 0,
      },
    ],
  });

  //make the Datepicker availabe before setting the values into data.period_start osv
  const [inEditMode, setInEditMode] = useState(false);
  const [loading, setLoading] = useState(false);
  const [inCommentMode, setInCommentMode] = useState(false);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(null);
  const [comment, setComment] = useState("");

  const [ineligiblePeriods, setIneligiblePeriods] = useState([]);

  const token = getStorage("token");
  let errorMessage = null;
  const history = useHistory();

  const paramId = useRef(props.match.params.id);

  //get value of the singleRequest
  useEffect(() => {
    setLoading(true);
    toastEndpoint(getIneligiblePeriods, (resp) =>
      setIneligiblePeriods(resp.data)
    );
    toastEndpoint(
      () => getSingleRequest(paramId.current),
      (resp) => {
        setData(resp.data);
        setLoading(false);
      }
    );
  }, []);

  //convert the value of id number to a string
  const statusHandler = (statusID) => {
    switch (statusID) {
      case 0:
        return "Pending";
      case 1:
        return "Approved";
      case -1:
        return "Denied";
      default:
        return "Unknown";
    }
  };
  //validate if the submit form is including a endDate when submiting. Else an errorMessege will be declared and return the validate as false
  const validate = () => {
    if (!data.period_end) {
      errorMessage = "Select an end date!";
    } else {
      if (!isDateRangeEligible(startDate, endDate, ineligiblePeriods)) {
        errorMessage =
          "The dates can not span over dates marked as ineligible!";
      }
    }
    if (errorMessage) {
      return false;
    }
    return true;
  };
  const validateTitle = () => {
    if (!data.title) {
      errorMessage = "Please enter a title!";
    }

    if (errorMessage) {
      return false;
    }
    return true;
  };

  const handleStatus = (status) => {
    if (status === 1) {
      setData((prevState) => ({
        ...prevState,
      }));
      data.status_id = 1;
    }
    if (status === -1) {
      setData((prevState) => ({
        ...prevState,
      }));
      data.status_id = -1;
    }
    saveChanges();
  };
  const saveChanges = () => {
    const isValid = validate();
    const isValidTitle = validateTitle();
    if (!isValid || !isValidTitle) {
      alert(errorMessage);
    }
    //Patch the id and obj to enpoint for setUpdatedVacationRequest
    if (isValid && isValidTitle) {
      alert("Request has been updated!");
      setInEditMode(false);
      toastEndpoint(() =>
        setUpdatedVacationRequest(data.id, {
          title: data.title,
          period_start: moment(data.period_start).format("YYYY-MM-DD"),
          period_end: moment(data.period_end).format("YYYY-MM-DD"),
          status_id: data.status_id,
          tidsbankenUser: { id: user.id }, // TODO: This user needs to be updated
        })
      );
    }
  };

  const onPostComment = () => {
    toastEndpoint(
      () =>
        postNewComment(data.id, {
          message: comment,
        }),
      (c) => {
        setComment("");
        setInCommentMode(false);
        toastEndpoint(
          () => getSingleRequest(paramId.current),
          (resp) => {
            setData(resp.data);
          }
        );
      }
    );
  };
  //change the start and end date first. then Set the values into data.
  const onChangeDate = ([start, end]) => {
    setStartDate(start);
    setEndDate(end);
    setData({ ...data, period_start: start, period_end: end });
  };
  const handleDelete = () => {
    const confirmDelete = global.confirm(
      "Are you sure you want to delete this request?"
    );
    if (confirmDelete) {
      toastEndpoint(
        () => deleteRequest(data.id),
        () => history.replace("/")
      );
    }
  };
  const getDayClassName = (date) => {
    return isDateEligible(date, ineligiblePeriods)
      ? undefined
      : "btn-secondary";
  };
  if (loading) {
    return <h1>Loading</h1>;
  }
  return (
    <div className="pt-5">
      {!token && <Redirect to="/login" />}
      <div>
      <div align="center">
        <Table striped bordered hover>
          <thead align="center">
            <tr>
              <th>
                <Link to={`/viewuser/${data.tidsbankenUser.id}`}>
                  {" "}
                  {data.tidsbankenUser.first_name}{" "}
                  {data.tidsbankenUser.last_name}{" "}
                </Link>{" "}
              </th>
            </tr>
          </thead>
          <tbody align="center">
            <tr>
              <td>
                {inEditMode ? (
                  <form>
                    <input
                      required
                      type="text"
                      value={data.title}
                      onChange={(ev) =>
                        setData({ ...data, title: ev.target.value })
                      }
                    />
                  </form>
                ) : (
                  `Title: ${data.title}`
                )}
              </td>
            </tr>
            {inEditMode ? (
              <tr>
                <td>
                  <DatePicker
                    minDate={moment().toDate()}
                    selected={startDate}
                    onChange={onChangeDate}
                    startDate={startDate}
                    endDate={endDate}
                    dayClassName={getDayClassName}
                    selectsRange
                    inline
                  />
                </td>
              </tr>
            ) : (
              <tr>
                <td>From: {moment(data.period_start).format("YYYY-MM-DD")}</td>
              </tr>
            )}
            {inEditMode ? (
              <tr></tr>
            ) : (
              <tr>
                <td>To: {moment(data.period_end).format("YYYY-MM-DD")}</td>
              </tr>
            )}
            <tr>
              <td>Status: {statusHandler(data.status_id)}</td>
            </tr>
            {(data.tidsbankenUser.id === user.id || user.is_admin) && (
              <React.Fragment>
                <tr>
                  <td className="bg-warning">Comments:</td>
                </tr>
                {!loading &&
                  [...data.comments]
                    .sort((a, b) => {
                      return a.time_created > b.time_created;
                    })
                    .map((item, i) => (
                      <tr key={i}>
                        <td>
                          <b>
                            {item.tidsbankenUser.first_name}{" "}
                            {item.tidsbankenUser.last_name}
                          </b>
                          : {item.message}{" "}
                        </td>
                      </tr>
                    ))}
              </React.Fragment>
            )}
            {inCommentMode && (
              <tr>
                <td>
                  New comment:
                  <input
                    minLength="1"
                    type="text"
                    value={comment}
                    onChange={(ev) => setComment(ev.target.value)}
                  />
                </td>
              </tr>
            )}
          </tbody>
        </Table>
        {/* If status still is pending and ineditmode is not true */}
        <div>
          {!inEditMode && !inCommentMode && (
            <React.Fragment>
              {data.status_id === 0 &&
                user.is_admin &&
                data.tidsbankenUser.id !== user.id && (
                  <React.Fragment>
                    <Button
                      variant="success"
                      onClick={() => {
                        handleStatus(1);
                      }}
                    >
                      Approve request
                    </Button>
                    <Button
                      variant="danger"
                      onClick={() => {
                        handleStatus(-1);
                      }}
                    >
                      Reject request
                    </Button>
                  </React.Fragment>
                )}
              <br />
              {/* Admin can only make changes on requests after approval */}
              {((data.tidsbankenUser.id === user.id && data.status_id === 0) ||
                user.is_admin) && (
                <React.Fragment>
                  <Button variant="warning" onClick={() => setInEditMode(true)}>
                    Edit Request
                  </Button>
                </React.Fragment>
              )}
            </React.Fragment>
          )}
          {user.is_admin &&
            !inEditMode &&
            !inCommentMode &&
            data.tidsbankenUser.id !== user.id && (
              <Button
                variant="danger"
                onClick={() => {
                  handleDelete();
                }}
              >
                Delete Request
              </Button>
            )}
          {(data.tidsbankenUser.id === user.id || user.is_admin) &&
            !inEditMode &&
            !inCommentMode && (
              <React.Fragment>
                <Button
                  variant="secondary"
                  onClick={() => setInCommentMode(true)}
                >
                  Add Comment
                </Button>{" "}
                <br />
                <br />
              </React.Fragment>
            )}

          {/* if the inEditMoe is true or false. different buttons will be availbe for the user */}
          {inEditMode && (
            <React.Fragment>
              <Button variant="success" onClick={() => saveChanges(data)}>
                Save
              </Button>

              <Button variant="danger" onClick={() => setInEditMode(false)}>
                Cancel
              </Button>
            </React.Fragment>
          )}
          {inCommentMode && (
            <React.Fragment>
              <Button variant="success" onClick={() => onPostComment()}>
                {" "}
                Post Comment
              </Button>
              <Button variant="danger" onClick={() => setInCommentMode(false)}>
                Cancel
              </Button>
            </React.Fragment>
          )}
        </div><br/><br/>
      </div>
          {!inEditMode && !inCommentMode && (
            <Button align="left" type="button" onClick={() => history.goBack()}>
              Back
            </Button>
          )}
    </div>
    </div>
  );
};
export default EditRequestByUser;
