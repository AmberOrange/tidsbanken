package se.experis.tidsbanken.models;

/**
 * Common Response class. Acts as a model for what is going to be sent back to the client when a request is sent to an endpoint.
 */
public class CommonResponse {
    public Object data;
    public String message;
}
