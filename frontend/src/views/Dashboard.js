import React, { useEffect, useRef, useState } from "react";
import { Row } from "react-bootstrap";
import Calendar from "../components/Calendar/Calendar";
import moment from "moment";

import "../components/Calendar/Calendar.css";
import CalendarNavbar from "../components/Calendar/CalendarNavbar";
import { getIneligiblePeriodWithinDateRange, getRequestsWithinDateRange } from "../api/EndPoints";
import IneligibleModal from "../components/Dashboard/IneligibleModal";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";


const Dashboard = () => {
  // State hooks
  const [currentMonth, setCurrentMonth] = useState(
    generateCurrentMonth(moment().startOf("month"))
  );
  const [requests, setRequests] = useState([]);
  const [ineligible, setIneligible] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const user = useSelector(state => state.user);
  // This request list is filled with test data
  // temporarily to show its functionality
  const calendarBodyRef = useRef();

  // Functions
  function generateCurrentMonth(month) {
    // Set the starting day index on a monday,
    // going backwards into previous month if needed
    let startDay = month.clone();
    // moment.js starts the week on a sunday, so the
    // date needs to be adjusted by adding six days
    // and make the date loop back around using modulo
    startDay.subtract((month.day() + 6) % 7, "days");

    // Set the date when we've reached the end of a
    // calendar page
    let endDay = month.clone().endOf("month");

    // Like dayIndex, adjust for week starting on a
    // monday
    // If the last day of the calendar page happens
    // to be a sunday, there's no need to go into the
    // next month
    const endOfWeekIndex = (endDay.day() + 6) % 7;
    if (endOfWeekIndex !== 6) {
      endDay.add(6 - endOfWeekIndex, "days");
    }

    return {
      month,
      startDay,
      endDay,
    };
  }

  const nextMonth = () => {
    setCurrentMonth(
      generateCurrentMonth(currentMonth.month.clone().add(1, "month"))
    );
    scrollToTop();
  };

  const previousMonth = () => {
    setCurrentMonth(
      generateCurrentMonth(currentMonth.month.clone().subtract(1, "month"))
    );
    scrollToTop();
  };

  const scrollToTop = () => {
    calendarBodyRef.current.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  const refreshPage = () => {
    async function fetchRequests() {
      try {
        const requestResponse = await getRequestsWithinDateRange(
          currentMonth.startDay.format("YYYY-MM-DD"),
          currentMonth.endDay.format("YYYY-MM-DD")
        );
        const ineligibleResponse = await getIneligiblePeriodWithinDateRange(
          currentMonth.startDay.format("YYYY-MM-DD"),
          currentMonth.endDay.format("YYYY-MM-DD")
        );
        if (isMounted) {
          setRequests(requestResponse.data);
          setIneligible(ineligibleResponse.data);
        }
      } catch (e) {
        toast.error(e);
      }
    }

    fetchRequests();
  }

  const onRefresh = () => {
    refreshPage();
  }

  // Lifecycle
  const isMounted = useRef(true);
  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    refreshPage();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentMonth]);

  // All views should be contained within a row using
  // the classes "flex-grow-1 overflow-auto"

  //
  return (
    <React.Fragment>
      <Row className="flex-grow-1 overflow-auto" ref={calendarBodyRef}>
        {user.is_admin && (
            <IneligibleModal
              show={showModal}
              onHide={() => setShowModal(false)}
              onRefresh={onRefresh}
              />
        )}
        <table id="calendar">
          <tbody>
            <tr className="weekdays">
              <th scope="col">Monday</th>
              <th scope="col">Tuesday</th>
              <th scope="col">Wednesday</th>
              <th scope="col">Thursday</th>
              <th scope="col">Friday</th>
              <th scope="col">Saturday</th>
              <th scope="col">Sunday</th>
            </tr>
            <Calendar month={currentMonth} requests={requests} ineligible={ineligible} />
          </tbody>
        </table>
      </Row>
      <div>
        <CalendarNavbar
          previous={previousMonth}
          next={nextMonth}
          currentMonth={currentMonth}
          ineligible={() => setShowModal(true)}
        />
      </div>
    </React.Fragment>
  );
};

export default Dashboard;
