import { DELETE_USER, SET_USER } from "./action.types";

export const setUserAction = (user) => ({
  type: SET_USER,
  user: user
});

export const deleteUserAction = () => ({
  type: DELETE_USER
});