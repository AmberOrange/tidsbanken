package se.experis.tidsbanken.utils;

// using SendGrid's Java Library
// https://github.com/sendgrid/sendgrid-java

import com.sendgrid.*;

import java.io.IOException;
import java.util.HashMap;

/**
 * EmailVerification class. This class handles two-factor validation of a user.
 *
 * @author - Karl Löfquist
 */
public class EmailVerification {

    /**
     * This method sends a verification email to the user. The email contains a code to be entered into the client
     * for user verification
     *
     * @param emailAddress - Email address of user to be logged in
     * @param name - TidsbankenUser name to be sent in email
     * @param code - Provided 2fa code to be entered by user to login
     * @throws IOException - Exception to be thrown if the email could not be sent for some reason
     */
    public static void sendVerification(String emailAddress, String name, String code) throws IOException {
        Email from = new Email("kallelofquist@hotmail.com");
        String subject = "Tidsbanken 2FA";
        Email to = new Email(emailAddress);
        String templateID = "d-27cdb0ae67464d92a89ce56e05e220e1";
        Mail mail = new Mail();

        DynamicPersonalization dynamicPersonalization = new DynamicPersonalization();
        dynamicPersonalization.addTo(to);
        dynamicPersonalization.setSubject(subject);
        dynamicPersonalization.dynamicTemplateData = new HashMap<>();
        dynamicPersonalization.dynamicTemplateData.put("first_name", name);
        dynamicPersonalization.dynamicTemplateData.put("code", code);

        mail.setFrom(from);
        mail.setTemplateId(templateID);
        mail.addPersonalization(dynamicPersonalization);

        // API key - should not be hard coded in the final product
        PropertiesReader reader = new PropertiesReader("sendgrid.env");
        String property = reader.getProperty("SENDGRID_API_KEY");
        SendGrid sg = new SendGrid(property);

        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            throw ex;
        }
    }
}