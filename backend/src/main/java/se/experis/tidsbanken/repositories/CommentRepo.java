package se.experis.tidsbanken.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import se.experis.tidsbanken.models.Comment;

import java.util.Calendar;
import java.util.List;

/**
 * This class converts Comment objects into database records and vice versa.
 * Also contains custom query for special operations needed in the program
 *
 * @author Karl Löfquist
 */
public interface CommentRepo extends JpaRepository<Comment, Integer> {
    /**
     * Get a specific Comment by id
     *
     * @param id - Id of Comment
     * @return - specific Comment object
     */
    Comment getById(Integer id);

    /**
     * Custom query for finding all comments related to specified request
     *
     * @param request_id - Integer - request_id initially provided by client
     * @return - List of found comments
     */
    @Query("SELECT c FROM Comment c WHERE c.vacationRequest.id = ?1")
    List<Comment> findCommentsByRequestId(Integer request_id);

    @Query("SELECT c FROM Comment c WHERE c.vacationRequest.id = ?1 ORDER BY c.time_created DESC")
    List<Comment> orderFoundCommentsByTime(Integer request_id);

    /**
     * Custom query for updating a comment with specified id
     *
     * @param id - Integer - id of specific comment to be updated
     * @param message - String - new message to be set as message in comment
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update Comment c set c.message = :message where c.id = :id")
    int updateMessage(@Param("id") Integer id, @Param("message") String message);

    /**
     * Custom query for updating a comment with specified id
     *
     * @param id - Integer - id of specific comment to be updated
     * @param timeEdited - Calendar - new time to be set as time_edited in comment
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update Comment c set c.time_edited = :timeEdited where c.id = :id")
    int updateTimeEdited(@Param("id") Integer id, @Param("timeEdited") Calendar timeEdited);

    /**
     * Custom query for finding all comments created at specified time
     *
     * @param fromDate - Calendar - fromDate provided by client
     * @param user_id - Integer - user_id provided by client
     * @return - List of found comments
     */
    @Query("SELECT c FROM Comment c WHERE c.time_created >= ?1 and not c.tidsbankenUser.id = ?2")
    List<Comment> findCommentsByCreationDate(Calendar fromDate, Integer user_id);

    /**
     * Custom query for finding all comments created at specified time by specific user
     *
     * @param fromDate - Calendar - fromDate provided by client
     * @param user_id - Integer - user_id provided by client
     * @return - List of found comments
     */
    @Query("SELECT c FROM Comment c WHERE c.time_created >= ?1 and not c.tidsbankenUser.id = ?2 and c.vacationRequest.tidsbankenUser.id = ?2")
    List<Comment> findRequestAuthorCommentsByCreationDate(Calendar fromDate, Integer user_id);

}
