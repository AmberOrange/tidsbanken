import React, { useState } from "react";
import {
  Button,
  Col,
  FormControl,
  InputGroup,
  Row,
  Table,
} from "react-bootstrap";
import UserListItem from "./Admin/UserListItem";

const UserSearch = ({onClickUser, onClickAddUser, users}) => {

  const [searchTerm, setSearchTerm] = useState("");

  const onSearchChange = (event) => setSearchTerm(event.target.value);

  const onClickUserRow = (user) => {
    onClickUser(user);
  };

  return (
    <React.Fragment>
      {/* Search bar and add button */}
      <Row className="m-2">
        <Col>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="search-bar">
                <span role="img" aria-label="Search">
                  🔍
                </span>
              </InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="Search..."
              aria-label="Search"
              aria-describedby="search-bar"
              onChange={onSearchChange}
              value={searchTerm}
            />
          </InputGroup>
        </Col>
        <Col sm="auto">
          <Button variant="success" onClick={onClickAddUser}>Add new user</Button>
        </Col>
      </Row>

      {/* List of users */}
      <Row className="justify-content-center" style={{ overflowX: "auto" }}>
        <Table striped bordered hover className="w-auto">
          <thead>
            <tr>
              <th>Picture</th>
              <th>First name</th>
              <th>Last name</th>
              <th>Email</th>
              <th>2FA</th>
              <th>Admin</th>
            </tr>
          </thead>
          <tbody>
            {/* Filters away users that doesn't match */}
            {users
              .filter(
                (user) =>
                  user.first_name
                    .toLowerCase()
                    .includes(searchTerm.toLowerCase()) ||
                  user.last_name
                    .toLowerCase()
                    .includes(searchTerm.toLowerCase()) ||
                  user.user_mail.toLowerCase().includes(searchTerm.toLowerCase())
              )
              .map((user, index) => (
                <UserListItem key={index} click={() => onClickUserRow(user)} user={user} />
              ))}
          </tbody>
        </Table>
      </Row>
    </React.Fragment>
  );
};

export default UserSearch;
