package se.experis.tidsbanken.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Class Comment that is a model for the comment table in the database
 */
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Comment implements Comparator<Comment> {

    /**
     * Primary key. Identification for a specific instance of Comment
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    /**
     * String that represents the message that the comment contains
     */
    @Column(nullable = false)
    public String message;

    /**
     * Calendar object that represents a date for when the comment was created
     */
    @Column
    public Calendar time_created;

    /**
     * Calendar object that represents a date for when the comment was edited
     */
    @Column
    public Calendar time_edited;

    @JsonGetter("vacationRequest")
    public Map<String, Object> getRequestInfo() {
        Map<String, Object> temp = new HashMap<>();
        temp.put("id", vacationRequest.id);
        temp.put("title", vacationRequest.title);
        temp.put("author_id", vacationRequest.tidsbankenUser.id);
        return temp;
    }

    /**
     * Identification of request which the comment belongs to
     */
    @ManyToOne
    public VacationRequest vacationRequest;

    @JsonGetter("tidsbankenUser")
    public Map<String, Object> getUserInfo() {
        Map<String, Object> temp = new HashMap<>();
        temp.put("id", tidsbankenUser.id);
        temp.put("first_name", tidsbankenUser.first_name);
        temp.put("last_name", tidsbankenUser.last_name);
        return temp;
    }

    /**
     * Identification of user who created the comment
     */
    @ManyToOne
    public TidsbankenUser tidsbankenUser;

    /**
     * Method that sorts all comments after time_created
     *
     * @param o1 - Object 1 (Comment)
     * @param o2 - Object 2 (Comment)
     * @return - int that specifies which of the two objects are going to be placed above the other in the database
     */
    @Override
    public int compare(Comment o1, Comment o2) {
        return o2.time_created.compareTo(o1.time_created);
    }
}
