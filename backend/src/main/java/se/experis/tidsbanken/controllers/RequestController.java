package se.experis.tidsbanken.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.tidsbanken.models.CommonResponse;
import se.experis.tidsbanken.models.TidsbankenUser;
import se.experis.tidsbanken.models.VacationRequest;
import se.experis.tidsbanken.repositories.RequestRepo;
import se.experis.tidsbanken.repositories.UserRepo;
import se.experis.tidsbanken.utils.CommonException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static se.experis.tidsbanken.utils.HelpFunctions.*;

/**
 * Request controller. The controller supplies a number of endpoints for request handling to be used by the front-end application.
 * The endpoints provides information stored in the database.
 *
 * @author Karl Löfquist, Tora Haukka, Emil Grenebrant and Martin Sandström
 * @version 0.1.0
 */
@RestController
public class RequestController {

    @Autowired
    private RequestRepo requestRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TokenController tokenController;


    /**
     * Endpoint for getting all vacation requests or requests with periods within client specified time from database with method GET
     *
     * @param token - JWT token for user verification
     * @param startDate - client specified start date (Optional)
     * @param endDate - client specified end date (Optional)
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @GetMapping("/request")
    public ResponseEntity<CommonResponse> requestsDisplay(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                          @RequestParam(name = "startDate", required = false) String startDate,
                                                          @RequestParam(name = "endDate", required = false) String endDate) {

        List<VacationRequest> vacationRequestList;
        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        TidsbankenUser user;

        // Checking the token to see if the user is logged in and a valid user
        try {
            user = tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (startDate != null && endDate != null) {

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

            try {
                start.setTime(sdf.parse(startDate));
                end.setTime(sdf.parse(endDate));
            } catch (ParseException e) {
                cr.message = "Invalid request. Format parameters startDate and endDate like '2020-01-01'";
                response = HttpStatus.BAD_REQUEST;
                e.printStackTrace();
                return new ResponseEntity<>(cr, response);
            }

            // Find all requests that the user is allowed to view
            if(user.is_admin) {
                vacationRequestList = requestRepo.findRequestsBetweenDates(start, end);
            } else {
                vacationRequestList = requestRepo.findRequestsBetweenDatesAndUser(start, end, user.id);
            }

        }

        else if ((startDate != null && endDate == null) || (startDate == null && endDate != null)) {
            cr.message = "Invalid request. If parameters are used, both startDate and endDate must be defined";
            response = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(cr, response);
        }

        else {
            if(user.is_admin) {
                vacationRequestList = requestRepo.findAll();
            } else {
                vacationRequestList = requestRepo.findAllRequestsUser(user.id);
            }
        }

        cr.message = "Valid request";
        cr.data = vacationRequestList;
        response = HttpStatus.OK;

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Get specific vacation request by id with method GET
     *
     * @param token - JWT token for user verification
     * @param id - Integer - id of VacationRequest specified by client
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @GetMapping("/request/{id}")
    public ResponseEntity<CommonResponse> getRequestById(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                         @PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if (requestRepo.existsById(id)){
            VacationRequest vacationRequest = requestRepo.getById(id);
            // Make an authority check depending on status
            try {
                // If accepted or rejected, just check if the user is logged in:
                if(vacationRequest.status_id != 0) {
                    tokenController.getUserFromToken(token);
                // Else if pending, check if they're the author or admin:
                } else {
                    tokenController.getAuthorOrAdminFromToken(token, vacationRequest.tidsbankenUser.id);
                }
            } catch (CommonException e) {
                return e.getResponseEntity();
            }

            cr.data = vacationRequest;
            cr.message = "Request "+id+" found";
            resp = HttpStatus.OK;
        }else{
            cr.data = null;
            cr.message = "Request "+id+" does not exist";
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Endpoint for getting a list of vacation requests for a specific user. Users other than the author and admins will not see pending requests.
     * Uses method GET
     *
     * @param token - JWT token for user verification
     * @param id - Integer - client specified id of TidsbankenUser
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @GetMapping("/user-requests/{id}")
    public ResponseEntity<CommonResponse> getRequestsByUserId(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                              @PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus resp;
        TidsbankenUser user;

        // Checking the token to see if the user is logged in and a valid user
        try {
            user = tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if(userRepo.existsById(id)) {
            if(user.is_admin || user.id == id) {
                cr.data = userRepo.getById(id).requests;
                cr.message = "Returned all of the users requests";
            } else {
                cr.data = requestRepo.getNonPendingRequestsFromUser(id);
                cr.message = "Returning the users non-pending requests";
            }
            resp = HttpStatus.OK;
        } else {
            cr.message = "User with id "+id+" does not exist";
            resp = HttpStatus.NOT_FOUND;
        }


        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Endpoint for creating a request with method POST
     *
     * @param token - JWT token for user verification
     * @param request - VacationRequest data in request body
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @PostMapping("/request")
    public ResponseEntity<CommonResponse> createRequest(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                        @RequestBody VacationRequest request) {

        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus response;
        TidsbankenUser user;

        // Checking the token to see if the user is logged in and a valid user
        try {
            user = tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if(request.tidsbankenUser.id != user.id) {
            cr.message= "You may not post a request as another user";
            response = HttpStatus.FORBIDDEN;
            return new ResponseEntity<>(cr, response);
        }

        request.status_id = 0;
        request.time_created = Calendar.getInstance();

        //if the request body is bad, return http response
        if(!validateRequest(request)) {
            cr.message = "Request body not valid";
            response = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(cr, response);
        }

        trimCalendar(request.period_start);
        trimCalendar(request.period_end);

        //process
        request = requestRepo.save(request);

        cr.data = request;
        cr.message = "New vacation request with id: " + request.id;

        response = HttpStatus.CREATED;

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for updating a vacation request with method PATCH
     *
     * @param token - JWT token for user verification
     * @param id - client specified id of request to be updated
     * @param updatedRequest - updated VacationRequest object that contains information that the request is going to be updated with
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @PatchMapping("/request/{id}")
    public ResponseEntity<CommonResponse> patchRequestStatus(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                             @PathVariable Integer id, @RequestBody VacationRequest updatedRequest) {


        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus response;

        try {
            tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        int numberOfUpdatedColumns = 0;
        Calendar rightNow = Calendar.getInstance();

        if(requestRepo.existsById(id)) {
            VacationRequest vacationRequest = requestRepo.getById(id);
            if(vacationRequest.status_id == 0) {

                TidsbankenUser user;

                // Checking the token to see if the user is logged in and a valid user
                try {
                    user = tokenController.getAuthorOrAdminFromToken(token, vacationRequest.tidsbankenUser.id);
                } catch (CommonException e) {
                    return e.getResponseEntity();
                }

                if (updatedRequest.status_id == 1 || updatedRequest.status_id == -1) {
                    if(user.is_admin && user.id != vacationRequest.tidsbankenUser.id) {
                        vacationRequest.status_id = updatedRequest.status_id;
                        requestRepo.updateTimeUpdated(id, rightNow);

                        cr.message = "Status has been updated";
                        response = HttpStatus.OK;
                    } else {
                        cr.message = "Only an admin who is not the author may change this request status";
                        response = HttpStatus.FORBIDDEN;
                    }
                } else {
                    if (updatedRequest.title != null && !vacationRequest.title.equals(updatedRequest.title)) {
                        vacationRequest.title = updatedRequest.title;
                        numberOfUpdatedColumns++;
                    }

                    if (updatedRequest.period_start != null) {
                        trimCalendar(updatedRequest.period_start);
                        if (!isCalendarSameDate(vacationRequest.period_start, updatedRequest.period_start)) {
                            vacationRequest.period_start = updatedRequest.period_start;
                            numberOfUpdatedColumns++;
                        }
                    }

                    if (updatedRequest.period_end != null) {
                        trimCalendar(updatedRequest.period_end);
                        if (!isCalendarSameDate(vacationRequest.period_end, updatedRequest.period_end)) {
                            vacationRequest.period_end = updatedRequest.period_end;
                            numberOfUpdatedColumns++;
                        }
                    }

                    requestRepo.updateTimeUpdated(id, rightNow);

                    cr.message = numberOfUpdatedColumns + " number of columns changed";
                    response = HttpStatus.OK;
                }
            } else {
                cr.message = "The request status has been set and the request can no longer be modified";
                response = HttpStatus.FORBIDDEN;
            }
        } else {
            cr.message = "The request with id " + id + " does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for deleting a vacation request based on client specified id. Uses method DELETE
     *
     * @param token - JWT token for user verification
     * @param id - client specified id of specific VacationRequest
     * @return - response entity with response message (and data if ok) and HTTP status
     */
    @DeleteMapping("/request/{id}")
    public ResponseEntity<CommonResponse> deleteRequest(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                        @PathVariable Integer id) {

        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        //Checking the token to see if user has admin rights
        try {
            tokenController.getAdminFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (requestRepo.existsById(id)) {
            requestRepo.deleteById(id);
            cr.message = "Request with id "+id+" have been successfully removed!";
            response = HttpStatus.OK;
        }
        else {
            cr.message = "Request with id "+id+" was not found.";
            response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }
}
