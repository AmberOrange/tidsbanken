import React, { useEffect, useState } from "react";
import { Dropdown, Nav, Navbar } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import NavCustomMenu from "./Navheader/NavCustomMenu";
import NavCustomToggle from "./Navheader/NavCustomToggle";
import NotificationItem from "./Navheader/NotificationItem";
import { deleteStorage } from "./../utils/localStorage";
import { useDispatch, useSelector } from "react-redux";
import { deleteUserAction } from "../store/actions/user.actions";
import { getNotifications, toastEndpoint } from "../api/EndPoints";

const Navheader = () => {
  // State hooks
  const [notificationList, setNotificationList] = useState([]);

  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    toastEndpoint(getNotifications, (resp) => setNotificationList(resp.data));
  }, []);

  // If the user confirm logout, the deleteStorage will be called and clearing the token. Then push the user to login page
  const handleLogout = () => {
    const doLogout = global.confirm("Are you sure you want to logout?");
    if (doLogout) {
      deleteStorage("token");
      dispatch(deleteUserAction());
    }
  };
  // Render
  return (
    <Navbar bg="light">
      {/* The Brand and Router Links */}
      <Navbar.Brand>Tidsbanken</Navbar.Brand>
      <Navbar.Collapse>
        <Nav className="mr-auto align-items-center">
          <LinkContainer to="/">
            <Nav.Link>Calendar</Nav.Link>
          </LinkContainer>
          {user.is_admin && (
            <LinkContainer to="/admin">
              <Nav.Link>Admin</Nav.Link>
            </LinkContainer>
          )}
        </Nav>
      </Navbar.Collapse>

      {/* Notification menu 🔔 */}
      <Dropdown className="mr-4">
        <Dropdown.Toggle as={NavCustomToggle} id="dropdown-custom-components">
          <span role="img" aria-label="Notifications">
            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-bell" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2z" />
              <path fillRule="evenodd" d="M8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z" />
            </svg> ({notificationList.reduce((previous, current) => previous + (user.latest_active < current.date ? 1 : 0), 0)})
          </span>
        </Dropdown.Toggle>

        <Dropdown.Menu
          as={NavCustomMenu}
          style={{
            width: "400px",
            maxWidth: "80vw",
            height: "70vmin",
            overflowY: "scroll",
            overflowX: "hidden",
          }}
          alignRight
        >
          {notificationList
          .map(({ thumbnail, message, date, link }, index) => (
            <NotificationItem
              key={index}
              thumbnail={thumbnail}
              message={message}
              date={date}
              link={link}
              latest={user.latest_active}
            />
          ))}
        </Dropdown.Menu>
      </Dropdown>

      {/* User menu */}
      {/* Contains temporary placeholder data */}
      <Dropdown>
        <Dropdown.Toggle as={NavCustomToggle} id="dropdown-custom-components">
          <span
            className="mr-2 d-none d-sm-none d-md-inline"
            style={{ verticalAlign: "middle" }}
          >
            {user.is_admin && "🔑"} {user.first_name} {user.last_name}
          </span>
          <img src={user.profile_pic} alt="Profile" width="30px" />
        </Dropdown.Toggle>

        {/* Profile drop-down menu */}
        <Dropdown.Menu alignRight>
          <LinkContainer to={`/viewuser/${user.id}`}>
            <Dropdown.Item>View Profile</Dropdown.Item>
          </LinkContainer>
          <Dropdown.Divider />
          <Dropdown.Item onClick={handleLogout}>Log Out</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </Navbar>
  );
};

export default Navheader;
