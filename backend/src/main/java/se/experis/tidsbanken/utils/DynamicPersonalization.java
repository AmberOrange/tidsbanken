package se.experis.tidsbanken.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sendgrid.Personalization;

import java.util.Map;

/**
 * DynamicPersonalization class. Code fetched from github to fix issue with sending dynamic data in SendGrid template
 *
 * @author - Github user: nickneiman
 */
public class DynamicPersonalization extends Personalization {

    /**
     * Map of properties to be set in 'dynamic_template_data' in SendGrid request body
     */
    @JsonProperty("dynamic_template_data")
    public Map<String, String> dynamicTemplateData;

}