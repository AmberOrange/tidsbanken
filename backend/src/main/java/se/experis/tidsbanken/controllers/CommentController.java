package se.experis.tidsbanken.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.tidsbanken.models.Comment;
import se.experis.tidsbanken.models.CommonResponse;
import se.experis.tidsbanken.models.TidsbankenUser;
import se.experis.tidsbanken.models.VacationRequest;
import se.experis.tidsbanken.repositories.CommentRepo;
import se.experis.tidsbanken.repositories.RequestRepo;
import se.experis.tidsbanken.utils.CommonException;

import java.util.Calendar;
import java.util.List;

import static se.experis.tidsbanken.utils.HelpFunctions.editableComment;
import static se.experis.tidsbanken.utils.HelpFunctions.validateComment;

/**
 * Comment controller. The controller supplies a number of endpoints for comment handling to be used by the front-end application.
 * The endpoints provides information stored in the database.
 *
 * @author Karl Löfquist, Tora Haukka, Emil Grenebrant and Martin Sandström
 * @version 0.1.0
 */
@RestController
public class CommentController {

    @Autowired
    private CommentRepo commentRepo;

    @Autowired
    private RequestRepo requestRepo;

    @Autowired
    private TokenController tokenController;

    /**
     * Endpoint for getting all comments related to request (specified by id) with method GET
     *
     * @param token - JWT token for user verification
     * @param id - Request id specified by client from which comments are going to be fetched from
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @GetMapping("/request/{id}/comment")
    public ResponseEntity<CommonResponse> getComments (@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                       @PathVariable Integer id) {

        List<Comment> foundCommentList;
        VacationRequest vacationRequest = requestRepo.getById(id);
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        // Checking the token to see if the user us logged in and a valid user
        try {
            tokenController.getAuthorOrAdminFromToken(token, vacationRequest.tidsbankenUser.id);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (id > 0) {
            foundCommentList = commentRepo.orderFoundCommentsByTime(id);

            if (foundCommentList.size() > 0) {
                cr.data = foundCommentList;
                cr.message = "Comment(s) found related to request with id: " + id;
                response = HttpStatus.OK;
            }

            else if (vacationRequest == null) {
                cr.message = "Request with id " + id + " does not exist";
                response = HttpStatus.NOT_FOUND;
            }

            else {
                cr.message = "No comments found on request " + id;
                response = HttpStatus.NOT_FOUND;
            }
        }

        else {
            cr.message = "Request id not valid. Id must be grater than 0";
            response = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for saving a new comment associated to request (specified by id) to database with method POST
     *
     * @param token - JWT token for user verification
     * @param id - Id (specified by client) of vacation request that the comment is going to be associated to
     * @param comment - Comment object sent from the client to be created and saved to the database
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @PostMapping("/request/{id}/comment")
    public ResponseEntity<CommonResponse> postComment (@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                       @PathVariable Integer id, @RequestBody Comment comment) {

        /*TODO: A user should not have to specify request_id and time_created*/

        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        if(requestRepo.existsById(id)) {
            comment.vacationRequest = requestRepo.getById(id);

            TidsbankenUser user;

            // Checking the token to see if the user is logged in and a valid user
            try {
                user = tokenController.getAuthorOrAdminFromToken(token, comment.vacationRequest.tidsbankenUser.id);
            } catch (CommonException e) {
                return e.getResponseEntity();
            }

            comment.tidsbankenUser = user;

            comment.time_created = Calendar.getInstance();

            if (!validateComment(comment)) {
                cr.message = "Request body not valid";
                response = HttpStatus.BAD_REQUEST;
            } else {
                comment = commentRepo.save(comment);
                cr.data = comment;
                cr.message = "New comment with id " + comment.id + " created.";
                response = HttpStatus.CREATED;
            }
        } else {
            cr.message = "Request id not valid.";
            response = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for getting specific comment by id using method GET
     *
     * @param token - JWT token for user verification
     * @param id - Integer - The id for the request that the comment is posted on.
     * @param cid - Integer - The id for the specific comment.
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @GetMapping("/request/{id}/comment/{cid}")
    public ResponseEntity<CommonResponse> getComment(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                     @PathVariable Integer id, @PathVariable Integer cid) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        VacationRequest vacationRequest = requestRepo.getById(id);

        // Checking the token to see if the user us logged in and a valid user
        try {
            tokenController.getAuthorOrAdminFromToken(token, vacationRequest.tidsbankenUser.id);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (vacationRequest.comments != null) {
            response = HttpStatus.OK;
            for (Comment comment: vacationRequest.comments) {
                if (comment.id == cid) {
                    cr.data = commentRepo.findById(cid);
                    cr.message = "Comment with id "+cid+" found!";
                    response = HttpStatus.OK;
                    break;
                }
                else {
                    cr.data = null;
                    cr.message = "Comment with id "+cid+" was not found on request with id: "+id+".";
                    response = HttpStatus.NOT_FOUND;
                }
            }
        } 
        else {
            cr.data = null;
            cr.message = "Request with id "+id+" was not found.";
            response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for deleting a specific comment using method DELETE
     *
     * @param token - JWT token for user verification
     * @param id - Integer - the id of the request that the comment is posted on.
     * @param cid - Integer - the id of the specific comment.
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @DeleteMapping("/request/{id}/comment/{cid}")
    public ResponseEntity<CommonResponse> deleteComment(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                        @PathVariable Integer id, @PathVariable Integer cid) {

        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        Comment comment;
        TidsbankenUser user;
        VacationRequest vacationRequest = requestRepo.getById(id);

        // Checking the token to see if the user us logged in and a valid user
        try {
            user= tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (requestRepo.existsById(id) && commentRepo.existsById(cid)) {
            comment = commentRepo.getById(cid);
            if (user.id == comment.tidsbankenUser.id) {
                if (editableComment(commentRepo.getById(cid))) {
                    commentRepo.deleteById(cid);
                    cr.message = "Comment with id: "+cid+" has been successfully removed!";
                    response = HttpStatus.OK;
                }
                else {
                    cr.message = "Comment with id: "+cid+" has not been removed. It has been more than 24h since it was created.";
                    response = HttpStatus.FORBIDDEN;
                }
            }
            else {
                cr.message ="Only the author of this comment can delete it.";
                response = HttpStatus.UNAUTHORIZED;
            }
        }
        else {
            cr.message = "Comment with id: "+cid+" was not found";
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for updating a comment with method PATCH
     *
     * @param token - JWT token for user verification
     * @param id - Integer - The id of the request that the comment is posted on.
     * @param cid - Integer - The id of the specific comment.
     * @param updateComment - Updated comment object that contains the information that the comment is going to be updated with.
     * @return - Response entity with response message (and data if ok) and HTTP status.
     */
    @PatchMapping("/request/{id}/comment/{cid}")
    public ResponseEntity<CommonResponse> patchComment(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                       @PathVariable Integer id, @PathVariable Integer cid,
                                                       @RequestBody Comment updateComment) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        VacationRequest vacationRequest = requestRepo.getById(id);
        Comment comment;
        TidsbankenUser user;

        int numberOfUpdatedColumns = 0;
        Calendar rightNow = Calendar.getInstance();

        // Checking the token to see if the user us logged in and a valid user
        try {
            user = tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }


        if (requestRepo.existsById(id) && commentRepo.existsById(cid)) {
            comment = commentRepo.getById(cid);
            if (user.id == comment.tidsbankenUser.id) {

                if (editableComment(commentRepo.getById(cid))) {
                    if (updateComment.message != null) {
                        numberOfUpdatedColumns += commentRepo.updateMessage(cid, updateComment.message);
                    }
                    if (numberOfUpdatedColumns == 0) {
                        cr.message = "No column updated. Index might be out of bounds or request body is invalid";
                        response = HttpStatus.BAD_REQUEST;
                    }
                    else {
                        commentRepo.updateTimeEdited(cid, rightNow);
                        //cr.data = commentRepo.getById(cid); //This shows the previous updated comment, not the current update. "Bug"
                        cr.message = "Updated comment with id: "+cid+" . Updated columns: "+numberOfUpdatedColumns;
                        response = HttpStatus.OK;
                    }
                }
                else {
                    cr.data = commentRepo.getById(cid);
                    cr.message = "Comment with id: "+cid+" not updated. It's been 24h since it was created";
                    response = HttpStatus.FORBIDDEN;
                }
            }
            else {
                cr.message = "This comment can only be edited by its author.";
                response = HttpStatus.UNAUTHORIZED;

            }
        }
        else {
            cr.message = "Comment with id : "+cid+" not found.";
            response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }
}
