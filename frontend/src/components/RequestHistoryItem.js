import moment from "moment";
import React from "react";
import { useHistory } from "react-router-dom";

const RequestHistoryItem = ({request: {id, title, period_start, period_end, status_id}}) => {
  const history = useHistory();

  const statusHandler = (statusID) => {
    switch (statusID) {
      case 0:
        return "Pending";
      case 1:
        return "Accepted";
      case -1:
        return "Rejected";
      default:
        return "Unknown";
    }
  };
  return (
    <tr onClick={() => history.push(`/editrequest/${id}`)} style={{ cursor: "pointer" }}>
      <td>{title}</td>
      <td>{moment(period_start).format("YYYY-MM-DD")}</td>
      <td>{moment(period_end).format("YYYY-MM-DD")}</td>
      <td>{statusHandler(status_id)}</td>
    </tr>
  );
};

export default RequestHistoryItem;
