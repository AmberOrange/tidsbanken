import React, { useEffect, useState } from "react";
import { Button, Col, FormControl, InputGroup, Row } from "react-bootstrap";
import UserModal from "../components/Admin/UserModal";
import UserSearch from "../components/UserSearch";
import { getAllRequests, getAllUsers, toastEndpoint } from "../api/EndPoints";
import { useHistory } from "react-router-dom";

const Admin = () => {
  const [maxVacationLength, setMaxVacationLength] = useState(14);
  const [showModal, setShowModal] = useState(false);
  const [modalEditMode, setModalEditMode] = useState(false);
  const [users, setUsers] = useState([]);
  
  const [requests, setRequests] = useState([]);
  
  const history = useHistory();

  const onRefresh = () => {
    toastEndpoint(
      getAllUsers,
      (resp) => setUsers(resp.data)
    );
    toastEndpoint(
      getAllRequests,
      (resp) => {
        setRequests(resp.data);
      },)
  }

  useEffect(() => {
    onRefresh();
  }, []);

  const newUserModalInfo = {
    id: -1,
    profile_pic: "",
    first_name: "",
    last_name: "",
    user_mail: "",
    is_admin: false,
    twoFA_is_active: false,
    user_password: ""
  }

  const [userModalInfo, setUserModalInfo] = useState(newUserModalInfo);

  const onChangeVacationLength = (event) =>
    setMaxVacationLength(event.target.value);

  const onClickUser = (userData) => {
    setUserModalInfo(userData);
    setModalEditMode(true);
    setShowModal(true);
  };

  const onClickAddUser = () => {
    setUserModalInfo(newUserModalInfo);
    setModalEditMode(false);
    setShowModal(true);
  };

  const exportToFile = () => {

    var jsonse = JSON.stringify(requests); 
    var blob1 = new Blob([jsonse], {type: "application/json"});
    var url = window.URL.createObjectURL(blob1);
    var a = document.createElement('a');

    a.href = url;
    a.download = "backup.json";
    a.click();
    window.URL.revokeObjectURL(url);
  }

  return (
    <div>
      <Row className="flex-grow-1 overflow-auto">
        <UserModal
          show={showModal}
          onHide={() => setShowModal(false)}
          modalEditMode={modalEditMode}
          userModalInfo={userModalInfo}
          setUserModalInfo={setUserModalInfo}
          onRefresh={onRefresh}
        />
        <Col md="8">
          <h2 className="mt-3 mb-3 text-center">Manage Users</h2>
          <UserSearch onClickUser={onClickUser} onClickAddUser={onClickAddUser} users={users} />
        <Button align="center" onClick={() => history.goBack()}>Back</Button>
        </Col>
        <Col md="4" style={{ backgroundColor: "lightgray" }}>
          <h2 className="mt-3 mb-3 text-center">Application Settings</h2>
          <Row className="m-3">
            <label htmlFor="vacation-length">
              Maximum vacation length in days:
            </label>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="vacation-length">
                  <span role="img" aria-label="Days">
                    📆
                  </span>
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="i.e. 14"
                aria-label="Days"
                aria-describedby="vacation-length"
                value={maxVacationLength}
                onChange={onChangeVacationLength}
              />
            </InputGroup>
          </Row>
          <Row className="m-3">
            <Col>
              <Button varity="success" block>
                Import JSON
              </Button>
            </Col>
            <Col>
              <Button id="json" type="button" varity="success" block onClick={exportToFile}>
                Export JSON
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default Admin;
