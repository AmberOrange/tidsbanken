import React from "react";
import { Button, Row } from "react-bootstrap";
import { getMainPoint, promiseEndpoint, toastEndpoint } from "../api/EndPoints";

// Purpose: To test error handling of a failed api call
const TestRequests = () => {
  const sendTestRequestAsync = async () => {
    try {
      alert(await getMainPoint());
    } catch(e) {
      alert(e);
    }
  };

  const sendTestRequestCallback = () => {
    promiseEndpoint(getMainPoint)
      .then(resp => alert("Success: " + resp))
      .catch(err => alert("Error: " + err));
  }

  const sendTestRequestToast = () => {
    toastEndpoint(getMainPoint, (result) => alert(result));
  }

  return(
    <Row className="flex-grow-1 overflow-auto">
      <Button variant="success" onClick={sendTestRequestAsync}>
        Send Test Request (Async)
      </Button>
      <Button variant="warning" onClick={sendTestRequestCallback}>
        Send Test Request (Callback)
      </Button>
      <Button variant="primary" onClick={sendTestRequestToast}>
        Send Test Request (Toast message)
      </Button>
    </Row>
  )
};

export default TestRequests;