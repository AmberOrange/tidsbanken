package se.experis.tidsbanken.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Comparator;

/**
 * Class IneligiblePeriod that is a model for the ineligible_period table in the database
 */
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class IneligiblePeriod implements Comparator<IneligiblePeriod> {

    /**
     * Primary key. Identification for a specific instance of IneligiblePeriod
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    /**
     * Calendar object that represents a date for when the period is starting
     */
    @Column(nullable = false)
    public Calendar period_start;

    /**
     * Calendar object that represents a date for when the period is ending
     */
    @Column(nullable = false)
    public Calendar period_end;

    /**
     * Calendar object that represents a date for when the ineligible period was created
     */
    @Column(nullable = false)
    public Calendar time_created;

    /**
     * Identification of admin user who created or updated the ineligible period
     */
    @Column(nullable = false)
    public int admin_id;

    /**
     * Method that sorts all ineligible periods after period_start
     *
     * @param o1 - Object 1 (IneligiblePeriod)
     * @param o2 - Object 2 (IneligiblePeriod)
     * @return - int that specifies which of the two objects are going to be placed above the other in the database
     */
    @Override
    public int compare(IneligiblePeriod o1, IneligiblePeriod o2) {
        return o1.period_start.compareTo(o2.period_start);
    }
}
