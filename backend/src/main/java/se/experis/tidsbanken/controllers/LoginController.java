package se.experis.tidsbanken.controllers;

import org.json.simple.JSONObject;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import se.experis.tidsbanken.models.CommonResponse;
import se.experis.tidsbanken.models.TidsbankenUser;
import se.experis.tidsbanken.repositories.UserRepo;
import se.experis.tidsbanken.utils.EmailVerification;
import se.experis.tidsbanken.utils.JwtTool;

import java.io.IOException;
import java.util.Calendar;
import java.util.Random;

/**
 * Login controller. The controller supplies an endpoint for user login handling to be used by the front-end application.
 *
 * @author Karl Löfquist, Tora Haukka, Emil Grenebrant and Martin Sandström
 * @version 0.1.0
 */
@RestController
public class LoginController {

    @Autowired
    private UserRepo userRepo;

    /**
     * Endpoint for validating a user to be logged in. Uses method POST
     *
     * @param user - TidsbankenUser object (containing user_mail and user_password) to be verified against existing user in database
     * @return - Response entity with common response (contains data and message) and HTTP status response
     */
    @PostMapping("/login")
    public ResponseEntity<CommonResponse> loginUser(@RequestBody TidsbankenUser user) {

        /*TODO: Stop too many login attempts (if they all fail)*/

        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        if (!user.user_mail.isEmpty() && !user.user_password.isEmpty()) {

            TidsbankenUser validateUser = userRepo.findUserWithMail(user.user_mail);

            if(validateUser != null) {

                if (BCrypt.checkpw(user.user_password, validateUser.user_password)) {

                    if (validateUser.twoFA_is_active) {

                        if (user.twoFA_code == null) {
                            Random random = new Random();
                            validateUser.twoFA_code = String.format("%04d", random.nextInt(10000));
                            userRepo.save(validateUser);

                            try {
                                EmailVerification.sendVerification(validateUser.user_mail, validateUser.first_name, validateUser.twoFA_code);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            JSONObject json = new JSONObject();
                            json.put("twoFA", true);

                            cr.data = json;
                            cr.message = "User validated. Login with code in email";
                            response = HttpStatus.OK;
                        }

                        else {
                            if (validateUser.twoFA_code.equals(user.twoFA_code)) {
                                validateUser.twoFA_code = null;
                                validateUser.latest_active = validateUser.recent_login;
                                validateUser.recent_login = Calendar.getInstance();
                                userRepo.save(validateUser);

                                cr.data = JwtTool.createToken(validateUser.id);
                                cr.message = "User validated with user email: " + user.user_mail;
                                response = HttpStatus.OK;
                            }
                            else {
                                cr.message = "Code invalid";
                                response = HttpStatus.FORBIDDEN;
                            }
                        }

                    }

                    else {
                        validateUser.latest_active = validateUser.recent_login;
                        validateUser.recent_login = Calendar.getInstance();
                        userRepo.save(validateUser);

                        cr.data = JwtTool.createToken(validateUser.id);
                        cr.message = "User validated with user email: " + user.user_mail;
                        response = HttpStatus.OK;
                    }
                } else {
                    cr.message = "User validation failed";
                    response = HttpStatus.UNAUTHORIZED;
                }
            } else {
                cr.message = "User validation failed";
                response = HttpStatus.UNAUTHORIZED;
            }
        }

        else {
            cr.message = "Both email and password needs to be provided";
            response = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(cr, response);
    }
}
