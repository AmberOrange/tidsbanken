import React, { useEffect, useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import {
  postNewVacationRequest,
  getIneligiblePeriods,
  toastEndpoint,
} from "../../api/EndPoints";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { isDateEligible, isDateRangeEligible } from "../../utils/helperFunctions";
import { Button } from "react-bootstrap";

const AddRequestForm = () => {
  const user = useSelector((state) => state.user);
  const [title, setTitle] = useState("");

  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  const [isSending, setIsSending] = useState(false);

  const [ineligiblePeriods, setIneligiblePeriods] = useState([]);
  const [days, setDays] = useState(0);

  const history = useHistory();
  let errorMessage = null;
  //change the start and end values when the datepicker using the onChange function
  const onChange = ([start, end]) => {
    setStartDate(start);
    setEndDate(end);
    if(start&&end){
      var b = moment(start),
          a = moment(end),
          intervals = ["days"],
          out = [];
          var diff = a.diff(b, intervals[0]);
          b.add(diff, intervals[0]);
          out.push(diff+ 1 + " " + intervals[0]);
          
          setDays(out)
          return out.join(", ")
    }
  };
  //validate if the submit form is including a endDate when submiting. Else an errorMessege will be declared and return the validate as false
  const validate = () => {
    if (!endDate) {
      errorMessage = "Select an end date!";
    } else {
      if(!isDateRangeEligible(startDate, endDate, ineligiblePeriods)){
        errorMessage = "The dates can not span over dates marked as ineligible!";
      }
    }
    if (errorMessage) {
      return false;
    }
    return true;
  };
  //when the submit button is clicked, the handleSubmit will be called.
  const handleSubmit = (ev) => {
    ev.preventDefault();
    //calling our validate function
    let isValid = validate();

    //an confirmation alertbox will be returned if "isValid" will be true. The POST fetchfunction will also be executed with the values
    //in the "initialRequest" variable. Then redirected to the dashboard page.
    if (isValid) {
      setIsSending(true);

      toastEndpoint(
        () =>
          postNewVacationRequest({
            title: title,
            period_start: moment(startDate).format("YYYY-MM-DD"),
            period_end: moment(endDate).format("YYYY-MM-DD"),
            time_created: moment().format(),
            tidsbankenUser: { id: user.id },
          }),
        () => {
          history.push("/");
        },
        () => {
          setIsSending(false);
        }
      );
    }
    //if "isValid" is false, then a alertbox will be shown with the declared error message from the validate function.
    if (!isValid) {
      alert(errorMessage);
    }
  };

  useEffect(() => {
    toastEndpoint(getIneligiblePeriods, (resp) => setIneligiblePeriods(resp.data));
  }, []);

  

  const getDayClassName = (date) => {
    return isDateEligible(date, ineligiblePeriods) ? undefined : "btn-secondary";
  };

  return (
    <div  >
      <React.Fragment >

      <form align="center" onSubmit={handleSubmit}>
        <div >
          <label>Title: </label>
          <input
            required
            minLength="1"
            type="text"
            value={title}
            placeholder="Enter title of the request..."
            onChange={(ev) => {
              setTitle(ev.target.value);
            }}
            />
        </div>
        <div>
          <DatePicker
            minDate={moment().toDate()}
            //selected={startDate}
            onChange={onChange}
            startDate={startDate}
            endDate={endDate}
            dayClassName={getDayClassName}
            selectsRange
            inline
          />
          <p>{days}</p>
        </div>
        <br/>
        {/* for now, the status will be changed to pending, even if there is a error message returning */}
        <Button type="submit" variant="warning" disabled={isSending}>
          Send request
        </Button> <br/>
        
        {isSending && <p>Sending request...</p>}

      </form>
        <Button alignt="left" onClick={() => history.goBack()}>Back</Button>
            </React.Fragment>
    </div>
  );
};
export default AddRequestForm;
