import React from "react"; 

const NavCustomMenu = React.forwardRef(
  ({ children, style, className, "aria-labelledby": labeledBy }, ref) => {
    return (
      <div
        ref={ref}
        style={style}
        className={className}
        aria-labelledby={labeledBy}
      >
        <div className="text-center">
          <h3 className="ml-5 mr-5">Notifications</h3>
        </div>
        {children}
      </div>
    );
  }
);

export default NavCustomMenu;