import { FETCH_ERROR_SHOW_IN_CONSOLE } from "../environment/environment";

const errorHandler = (err) => {
  // If the error has a response, get the response message or statustext
  const message =
    (err.response && (err.response.data.message || err.response.statusText)) ||
  // Otherwise, convert the error to a json and retrieve the message
    err.toJSON().message;

  // Show the error in the console
  if(FETCH_ERROR_SHOW_IN_CONSOLE) {
    console.error(`Message: ${message}`);
  }
  
  throw message;
};

export default errorHandler;