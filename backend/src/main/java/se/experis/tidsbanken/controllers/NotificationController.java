package se.experis.tidsbanken.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import se.experis.tidsbanken.models.Comment;
import se.experis.tidsbanken.models.CommonResponse;
import se.experis.tidsbanken.models.TidsbankenUser;
import se.experis.tidsbanken.models.VacationRequest;
import se.experis.tidsbanken.repositories.CommentRepo;
import se.experis.tidsbanken.repositories.RequestRepo;
import se.experis.tidsbanken.repositories.UserRepo;
import se.experis.tidsbanken.utils.CommonException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@RestController
public class NotificationController {

    @Autowired
    private RequestRepo requestRepo;

    @Autowired
    private CommentRepo commentRepo;

    @Autowired
    private TokenController tokenController;

    private class Notification implements Comparable<Notification>{
        private String thumbnail;
        private String message;
        private Calendar date;
        private String link;

        public Notification(String thumbnail, String message, Calendar date, String link) {
            this.thumbnail = thumbnail;
            this.message = message;
            this.date = date;
            this.link = link;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Calendar getDate() {
            return date;
        }

        public void setDate(Calendar date) {
            this.date = date;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        @Override
        public int compareTo(Notification other) {
            return other.date.compareTo(this.date);
        }
    }

    /**
     * Endpoint for getting the current user by verifying the authorization token provided in the header.
     *
     * @param token - JWT token for user verification
     * @return - Response entity with adapted responses and data based on if the specific user were found or not
     */
    @GetMapping("/notifications")
    public ResponseEntity<CommonResponse> getRelevantNotifications(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token) {
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus resp;
        TidsbankenUser user;

        // Checking the token to see if the user has admin rights
        try {
            user = tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        Calendar fromDate = Calendar.getInstance();
        //fromDate.add(Calendar.DAY_OF_MONTH, -7);
        fromDate.add(Calendar.HOUR, -20);

        List<Notification> notificationList = new ArrayList<>();


        // Requests using time_created
        if(user.is_admin){
            List<VacationRequest> requestCreated = requestRepo.findRequestsCreatedSinceDate(fromDate);

            for(VacationRequest request : requestCreated) {
                notificationList.add(new Notification(
                        request.tidsbankenUser.profile_pic,
                        "New request \""+request.title+"\" by "+request.tidsbankenUser.first_name+" "+ request.tidsbankenUser.last_name,
                        request.time_created,
                        "/editrequest/"+request.id
                ));
            }
        }

        // Requests using time_updated
        {
            List<VacationRequest> requestUpdated;
            if(user.is_admin) {
                requestUpdated = requestRepo.findRequestsUpdatedSinceDate(fromDate);
            } else {
                requestUpdated = requestRepo.findUserRequestsUpdatedSinceDate(fromDate, user.id);
            }

            for(VacationRequest request : requestUpdated) {
                notificationList.add(new Notification(
                        request.tidsbankenUser.profile_pic,
                        "Request " + request.title +
                                (user.is_admin ? " by " + request.tidsbankenUser.first_name + " " + request.tidsbankenUser.last_name : "") +
                                " was " + (request.status_id == 0 ?
                                "edited" : request.status_id == 1 ? "approved" : "denied"),
                        request.time_created,
                        "/editrequest/"+request.id
                ));
            }
        }

        // Comments from the users requests
        {
            List<Comment> commentList;
            if(user.is_admin) {
                commentList = commentRepo.findCommentsByCreationDate(fromDate, user.id);
            } else {
                commentList = commentRepo.findRequestAuthorCommentsByCreationDate(fromDate, user.id);
            }

            for(Comment comment : commentList) {
                notificationList.add(new Notification(
                        comment.tidsbankenUser.profile_pic,
                       comment.tidsbankenUser.first_name + " has left a comment on" +
                               (user.id == comment.vacationRequest.tidsbankenUser.id ? " your " : " the ") +
                               "request named \"" + comment.vacationRequest.title + "\"",
                        comment.time_created,
                        "/editrequest/"+comment.vacationRequest.id
                ));
            }
        }

        // TODO: SORT
        Collections.sort(notificationList);


        cr.data = notificationList;
        cr.message = "Returning a list of notifications";
        resp = HttpStatus.OK;
        return new ResponseEntity<>(cr, resp);
    }

}
