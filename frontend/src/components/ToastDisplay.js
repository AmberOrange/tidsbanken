import React from "react";
import { ToastContainer, Slide } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ToastDisplay = () => {
  // Usage:
  // const notify = () => toast.error("Wow so easy!");

  return (
    <React.Fragment>
      <ToastContainer
        transition={Slide}
        position="top-right"
        autoClose={10000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </React.Fragment>
  );
};

export default ToastDisplay;
