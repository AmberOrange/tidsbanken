import React from "react";
import { LinkContainer } from "react-router-bootstrap";

const CalendarDay = ({ day, otherMonth, requests, ineligible }) => {
  const statusToText = (status) => {
    switch(status) {
      case -1:
        return "denied";
      case 1:
        return "approved";
      default:
        return "pending";
    }
  }
  return (
    // Use props to determine what style the day should have
    <td className={`day 
    ${otherMonth ? "other-month" : ""} 
    ${ineligible ? "ineligible" : ""}`}>
      <div className="date">{day}</div>

      {/* If there are requests, add them to the day */}
      {requests &&
        requests.map((item, index) => (
          <LinkContainer key={index} to={`/editrequest/${item.id}`}>
            <div className={`event event-${statusToText(item.status_id)}`}>
              <div className="event-desc">
                { item.title }
              </div>
              <div className="event-time">{item.tidsbankenUser.first_name} {item.tidsbankenUser.last_name}</div>
            </div>
          </LinkContainer>
        ))}
    </td>
  );
};

export default CalendarDay;
