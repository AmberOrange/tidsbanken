package se.experis.tidsbanken.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Class TidsbankenUser which acts as a model for table tidsbanken_user in database
 */
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class TidsbankenUser {

    /**
     * Primary key. Identification of specific instance of TidsbankenUser
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    /**
     * First name of a TidsbankenUser
     */
    @Column(nullable = false)
    public String first_name;

    /**
     * Last name of a TidsbankenUser
     */
    @Column(nullable = false)
    public String last_name;

    /**
     * Email address of a TidsbankenUser
     */
    @Column(nullable = false)
    public String user_mail;

    /**
     * Get password of a TidsbankenUser
     *
     * @return - Empty string
     */
    @JsonGetter("user_password")
    public String getUser_password() {
        return "";
    }

    /**
     * A TidsbankenUser's hashed password
     */
    @Column(nullable = false)
    public String user_password;

    /**
     * URL to profile picture of a TidsbankenUser
     */
    @Column
    public String profile_pic;

    /**
     * A TidsbankenUser can have admin privileges or not
     */
    @Column(nullable = false)
    public Boolean is_admin;

    /**
     * A Calander object for a TidsbankenUser's recent login
     */
    @JsonIgnore
    @Column
    public Calendar recent_login;

    /**
     * Calendar object that indicates when the TidsbankenUser was latest active
     */
    @Column
    public Calendar latest_active;

    /**
     * A TidsbankenUser can have 2FA activated or not
     */
    @Column(nullable = false)
    public Boolean twoFA_is_active;

    /**
     * TidsbankenUser two-factor authorization code
     */

    @Column
    public String twoFA_code;

    /**
     * Collection of comments created by this TidsbankenUser
     */
    @JsonIgnore
    @OneToMany(mappedBy = "tidsbankenUser", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    public Set<Comment> comments = new HashSet<>();

    /**
     * VacationRequest's owned by this TidsbankenUser
     */
    @JsonIgnore
    @OneToMany(mappedBy = "tidsbankenUser", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true)
    public Set<VacationRequest> requests = new HashSet<>();

}
