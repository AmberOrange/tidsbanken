package se.experis.tidsbanken.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.tidsbanken.models.CommonResponse;
import se.experis.tidsbanken.models.IneligiblePeriod;
import se.experis.tidsbanken.models.TidsbankenUser;
import se.experis.tidsbanken.repositories.IneligiblePeriodRepo;
import se.experis.tidsbanken.utils.CommonException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static se.experis.tidsbanken.utils.HelpFunctions.*;

/**
 * Ineligible period controller. The controller supplies a number of endpoints for ineligible period handling to be used by the front-end application.
 * The endpoints provides information stored in the database.
 *
 * @author Karl Löfquist, Tora Haukka, Emil Grenebrant and Martin Sandström
 * @version 0.1.0
 */
@RestController
public class IneligiblePeriodController {

    @Autowired
    private IneligiblePeriodRepo ineligibleRepo;

    @Autowired
    private TokenController tokenController;

    /**
     * Help method for checking the current (to be created) ineligible period against all ineligible periods that exist in the database.
     * If dates of new ineligible period (period_start & period_end) overlaps any of the dates in the already existing ineligible periods,
     * the new period should not be created.
     *
     * @param periodStart - Calendar object that represents ineligible period start of the period to be created
     * @param periodEnd - Calendar object that represents ineligible period end of the period to be created
     * @return - Boolean. True if dates do not overlap with another object's dates, otherwise false
     */
    private boolean checkDates(Calendar periodStart, Calendar periodEnd) {

        List<IneligiblePeriod> allIneligiblePeriods = ineligibleRepo.findAll();

        for (IneligiblePeriod ineligiblePeriod : allIneligiblePeriods) {

            if (((ineligiblePeriod.period_start.getTime().compareTo(periodStart.getTime())) <= 0 &&
                    (ineligiblePeriod.period_end.getTime().compareTo(periodStart.getTime())) >= 0) ||
                    ((ineligiblePeriod.period_start.getTime().compareTo(periodEnd.getTime())) <= 0 &&
                            (ineligiblePeriod.period_end.getTime().compareTo(periodEnd.getTime())) >= 0)) {

                System.out.println("CheckDate returns false on ineligible period with id: " + ineligiblePeriod.id);
                return false;
            }
        }
        return true;
    }

    /**
     * Endpoint for getting all ineligible periods or ineligible periods with period_start and period_end within client
     * specified time from database using method GET
     *
     * @param token - JWT token for user verification
     * @param startDate - client specified start date (optional)
     * @param endDate - client specified end date (optional)
     * @return - Response entity with common response (contains data and message) and HTTP status response
     */
    @GetMapping("/ineligible")
    public ResponseEntity<CommonResponse> getIneligiblePeriods(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                               @RequestParam(name = "startDate", required = false) String startDate,
                                                               @RequestParam(name = "endDate", required = false) String endDate) {

        List<IneligiblePeriod> ineligiblePeriodList;
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        // Checking the token to see if the user is logged in and a valid user
        try {
            tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (startDate != null && endDate != null) {

            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

            try {
                start.setTime(sdf.parse(startDate));
                end.setTime(sdf.parse(endDate));
            } catch (ParseException e) {
                cr.message = "Invalid request. Format parameters startDate and endDate like '2020-01-01'";
                response = HttpStatus.BAD_REQUEST;
                e.printStackTrace();
                return new ResponseEntity<>(cr, response);
            }

            //find IneligiblePeriods within dates using custom query in repository
            ineligiblePeriodList = ineligibleRepo.findIneligiblePeriodsBetweenDates(start, end);

        } else if ((startDate != null && endDate == null) || (startDate == null && endDate != null)) {
            cr.message = "Invalid request. If parameters are used, both startDate and endDate must be defined";
            response = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(cr, response);
        } else {
            ineligiblePeriodList = ineligibleRepo.sortIPByPeriodStart();
        }

        cr.message = "Valid request";
        cr.data = ineligiblePeriodList;
        response = HttpStatus.OK;

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for saving an ineligible period to the database with method POST
     *
     * @param token - JWT token for user verification
     * @param ineligiblePeriod - Ineligible period to be stored in the database
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @PostMapping("/ineligible")
    public ResponseEntity<CommonResponse> postIneligiblePeriod(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                               @RequestBody IneligiblePeriod ineligiblePeriod) {

        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        TidsbankenUser user;

        // Checking the token to see if the user is logged in and a valid user
        try {
            user = tokenController.getAdminFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        ineligiblePeriod.admin_id = user.id;

        //if the request body is bad, return http response
        if(!validateIneligiblePeriod(ineligiblePeriod)) {
            cr.message = "Request body not valid";
            response = HttpStatus.BAD_REQUEST;
        }
        else if (!checkDates(ineligiblePeriod.period_start, ineligiblePeriod.period_end)) {
            cr.message = "Period start and/or end is overlapping an already existing ineligible period";
            response = HttpStatus.FORBIDDEN;
        }
        else if (ineligiblePeriod.period_start.getTime().compareTo(ineligiblePeriod.period_end.getTime()) > 0) {
            cr.message = "Period end cannot be before period start";
            response = HttpStatus.FORBIDDEN;
        }
        else {

            trimCalendar(ineligiblePeriod.period_start);
            trimCalendar(ineligiblePeriod.period_end);

            // Set time_created to current time
            ineligiblePeriod.time_created = Calendar.getInstance();

            //process
            ineligiblePeriod = ineligibleRepo.save(ineligiblePeriod);

            cr.data = ineligiblePeriod;
            cr.message = "New ineligible period with id: " + ineligiblePeriod.id;

            response = HttpStatus.CREATED;
        }

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for deleting ineligible period by id specified by client with method DELETE.
     *
     * @param token - JWT token for user verification
     * @param id - Integer - id of the ineligible period.
     * @return - Response entity with message and HTTP status.
     */
    @DeleteMapping("/ineligible/{id}")
    public ResponseEntity<CommonResponse> deletePeriod(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                       @PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus response;

        // Checking the token to see if the user is logged in and a valid user
        try {
            tokenController.getAdminFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (ineligibleRepo.existsById(id)) {
            ineligibleRepo.deleteById(id);
            cr.message = "Ineligible period with id "+id+" has been deleted";
            response = HttpStatus.OK;
        }
        else {
            cr.message = "Ineligible period with id "+id+" was not found.";
            response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for getting specific ineligible period by id with method GET.
     *
     * @param token - JWT token for user verification
     * @param id - Integer - Id of ineligible period specified by client.
     * @return - Specific ineligible period object and a message if the id is found and
     * https status is OK. Else it will return null and an error message.
     */
    @GetMapping("/ineligible/{id}")
    public ResponseEntity<CommonResponse> getIPById(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                    @PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        // Checking the token to see if the user is logged in and a valid user
        try {
            tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (ineligibleRepo.existsById(id)) {
            cr.data = ineligibleRepo.findById(id);
            cr.message = "Ineligible period "+id+" found.";
            response = HttpStatus.OK;
        }
        else {
            cr.data = null;
            cr.message = "Ineligible period "+id+" not found";
            response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for updating ineligible period with client specified id with method PATCH
     *
     * @param token - JWT token for user verification
     * @param id - client specified id of the ineligible period to be updated
     * @param updatePeriod - IneligiblePeriod object sent from client that contains information to be updated in specified period
     * @return - Response entity with response message and HTTP status
     */
    @PatchMapping("/ineligible/{id}")
    public ResponseEntity<CommonResponse> patchIP(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token,
                                                  @PathVariable Integer id, @RequestBody IneligiblePeriod updatePeriod) {

        TidsbankenUser user;

        // Checking the token to see if the user is logged in and a valid user
        try {
            user = tokenController.getAdminFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        updatePeriod.admin_id = user.id;

        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        int numberOfUpdatedColumns = 0;

        if (ineligibleRepo.existsById(id)) {
            IneligiblePeriod currentPeriod = ineligibleRepo.getById(id);

            if (updatePeriod.period_start != null) {
                currentPeriod.period_start = updatePeriod.period_start;
                numberOfUpdatedColumns++;
            }
            if (updatePeriod.period_end != null) {
                currentPeriod.period_end = updatePeriod.period_end;
                numberOfUpdatedColumns++;
            }
            if (currentPeriod.admin_id != updatePeriod.admin_id) {
                currentPeriod.admin_id = updatePeriod.admin_id;
                numberOfUpdatedColumns++;
            }
            if (numberOfUpdatedColumns == 0) {
                cr.message = "Patch request not valid";
                response = HttpStatus.BAD_REQUEST;
            }
            else {
                ineligibleRepo.save(currentPeriod);
                cr.message = "Ineligible period with id "+id+" updated. Number of affected columns: " + numberOfUpdatedColumns;
                cr.data = ineligibleRepo.getById(id);
                response = HttpStatus.OK;
            }
        }
        else {
        cr.message = "No ineligible period updated. Id not found.";
        response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }
}