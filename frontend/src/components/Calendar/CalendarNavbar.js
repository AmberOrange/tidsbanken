import React from "react";
import {
  Button,
  Col,
  Row,
} from "react-bootstrap";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const CalendarNavbar = ({ previous, next, currentMonth, ineligible }) => {
  const user = useSelector(state => state.user);
  return (
    <React.Fragment>
      <Row style={{ backgroundColor: "#e0e1e2" }}>
        <Col md={6} className='mt-2 mb-2'>
          <Row className="justify-content-center align-items-center">
            <Col xs={2} className="d-flex justify-content-center">
              <Button variant="secondary"  size="lg" onClick={previous}>
                🡸
              </Button>
            </Col>
            <Col xs={6} className="d-flex justify-content-center">
              <h3 className='text-center'>{currentMonth.month.format("YYYY MMMM")}</h3>
            </Col>
            <Col xs={2} className="d-flex justify-content-center">
              <Button variant="secondary" size="lg" onClick={next}>
                🡺
              </Button>
            </Col>
          </Row>
        </Col>
        <Col md={6} className="mt-2 mb-2 d-flex justify-content-around align-items-center">
          <Link to="/addrequest">
            <Button variant="success" size="lg">
                  Add new vacation request
            </Button>
          </Link>
          {user.is_admin && (
            <Button variant="warning" size="lg" onClick={ineligible}>
                  Add ineligible period
            </Button>
          )}
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default CalendarNavbar;
