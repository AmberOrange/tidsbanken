import React from "react";
import AddRequestForm from "./forms/AddRequestForm";


const AddRequest = () => {

    //get token
    return (
        <div>
            <h1 align="center">New vacation request</h1>
            <AddRequestForm />
        </div>
    );
};
export default AddRequest;