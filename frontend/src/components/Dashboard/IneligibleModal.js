import React, { useEffect, useState } from "react";
import { Button, Form, Modal, Row } from "react-bootstrap";
import moment from "moment";
import ReactDatePicker from "react-datepicker";
import styles from "./IneligibleModal.module.css";
import { getIneligiblePeriods, postIneligiblePeriod, toastEndpoint } from "../../api/EndPoints";
import { toast } from "react-toastify";
import {
  isDateEligible,
  isDateRangeEligible,
} from "../../utils/helperFunctions";

const IneligibleModal = ({ show, onHide, onRefresh }) => {

  const [isProcessing, setIsProcessing] = useState(false);
  const [fromDate, setFromDate] = useState(null);
  const [toDate, setToDate] = useState(null);
  const [ineligiblePeriods, setIneligiblePeriods] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    toastEndpoint(getIneligiblePeriods, (resp) => setIneligiblePeriods(resp.data));
  }, []);

  const onClickSubmit = (ev) => {
    ev.preventDefault();
    if (!toDate) {
      setErrorMessage("A start date and an end date needs to be selected");
    } else if (!isDateRangeEligible(fromDate, toDate, ineligiblePeriods)) {
      setErrorMessage("The dates can not span over dates marked as ineligible");
    } else {
      setErrorMessage("");
      setIsProcessing(true);
      toastEndpoint(
        () => postIneligiblePeriod(moment(fromDate).format("YYYY-MM-DD"), moment(toDate).format("YYYY-MM-DD")),
        () => {
          toast.success("Successfully added ineligible period");
          onRefresh();
          setIsProcessing(false);
          setFromDate(null);
          setToDate(null);
          toastEndpoint(getIneligiblePeriods, (resp) => setIneligiblePeriods(resp.data));
          onHide();
        },
        () => {
          setIsProcessing(false);
        }
      );
    }
  };

  const onChange = ([from, to]) => {
    setFromDate(from);
    setToDate(to);
  };

  const getDayClassName = (date) => {
    return isDateEligible(date, ineligiblePeriods)
      ? undefined
      : "btn-secondary";
  };

  return (
    <Modal
      show={show}
      onShow={() => setErrorMessage("")}
      onHide={onHide}
      dialogClassName={styles.modalWidth}
      aria-labelledby="contained-modal-title-vcenter"
      animation={false}
      backdrop={isProcessing ? "static" : true}
      centered
    >
      <Modal.Header closeButton={!isProcessing}>
        <Modal.Title id="contained-modal-title-vcenter">
          Add ineligible period
        </Modal.Title>
      </Modal.Header>
      <Form onSubmit={onClickSubmit}>
        <Modal.Body className="m-3">
          <Row className="justify-content-center">
            <ReactDatePicker
            minDate={moment().toDate()}
            onChange={onChange}
            startDate={fromDate}
            endDate={toDate}
            dayClassName={getDayClassName}
            selectsRange
            inline
          />
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <i>
            <small style={{color:"red"}}>{errorMessage}</small>
          </i>
          <Button variant="primary" type="submit" disabled={isProcessing}>
            Submit
          </Button>
          <Button variant="secondary" onClick={onHide} disabled={isProcessing}>
            Close
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default IneligibleModal;
