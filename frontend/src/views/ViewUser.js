import React, { useEffect, useRef, useState } from "react";
import {
  Button,
  Col,
  Container,
  Form,
  FormControl,
  InputGroup,
  Row,
  Table,
} from "react-bootstrap";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import {
  getSingleUser,
  getUsersRequests,
  patchUser,
  postNewUserPassword,
  toastEndpoint,
} from "../api/EndPoints";
import RequestHistoryItem from "../components/RequestHistoryItem";

const ViewUser = (props) => {
  const fallbackPicture =
    "https://t3.ftcdn.net/jpg/00/64/67/80/240_F_64678017_zUpiZFjj04cnLri7oADnyMH0XBYyQghG.jpg";

  const [picture, setPicture] = useState("");
  const [data, setData] = useState({
    id: -1,
    first_name: "",
    last_name: "",
    user_mail: "",
    profile_pic: "",
    is_admin: true,
    latest_active: 0,
    twoFA_is_active: false,
  });
  const [requests, setRequests] = useState([]);
  const [password, setPassword] = useState("");
  const [controlPassword, setControlPassword] = useState("");
  const [hasChanges, setHasChanges] = useState(false);

  const paramId = useRef(parseInt(props.match.params.id));

  const history = useRef(useHistory());

  const goBackHistory = useHistory();

  const user = useSelector((state) => state.user);

  useEffect(() => {
    toastEndpoint(
      () => getSingleUser(paramId.current),
      (resp) => {
        setData(resp.data);
        setPicture(resp.data.profile_pic);
        toastEndpoint(
          () => getUsersRequests(resp.data.id),
          (secondResp) => {
            setRequests(secondResp.data);
          }
        );
      },
      () => history.current.push("/")
    );
  }, []);

  const onSubmit = (ev) => {
    ev.preventDefault();
    if (hasChanges) {
      toastEndpoint(
        () =>
          patchUser(paramId.current, {
            user_mail: data.user_mail,
            profile_pic: data.profile_pic,
            twoFA_is_active: data.twoFA_is_active,
          }),
        () => {
          if(password && controlPassword && password === controlPassword) {
            toastEndpoint(() => postNewUserPassword(paramId.current, {user_password: password}),
            () => toast.success("Changes have been saved!"),
            () => toast.warning("Changes except the change of password has been saved"))
          } else {
            toast.success("Changes have been saved!");
          }
          setHasChanges(false);
        }
      );
    }
  };

  return (
    <Container className="pt-3" style={{ maxWidth: "60em" }}>
      <Row className="justify-content-center">
        <h2>
          {data.first_name} {data.last_name}
        </h2>
      </Row>
      <hr />
      <Row>
        <Col sm="4" className="d-flex justify-content-center">
          <img
            src={picture}
            alt="Profile"
            className="img-fluid"
            onError={() => setPicture(fallbackPicture)}
          />
        </Col>
        <Col sm="8" className="pt-3">
          {paramId.current === user.id ? (
            <Form onSubmit={onSubmit}>
              <Row className="p-3">
                <InputGroup>
                  <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon1">Email</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    type="email"
                    aria-label="Email"
                    aria-describedby="basic-addon1"
                    value={data.user_mail}
                    onChange={(ev) => {
                      setData({ ...data, user_mail: ev.target.value });
                      setHasChanges(true);
                    }}
                    required
                  />
                </InputGroup>
                <InputGroup>
                  <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon4">
                      Profile Picture
                    </InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    aria-label="Profile"
                    aria-describedby="basic-addon4"
                    value={data.profile_pic}
                    onChange={(ev) => {
                      setData({ ...data, profile_pic: ev.target.value });
                      setHasChanges(true);
                    }}
                    onBlur={() => setPicture(data.profile_pic)}
                  />
                </InputGroup>
              </Row>
              <Row className="p-3">
                <span className="p-2">Change password:</span>
                <InputGroup>
                  <FormControl
                    type="password"
                    aria-label="Password"
                    aria-describedby="basic-addon2"
                    placeholder="Enter new password..."
                    value={password}
                    onChange={(ev) => {
                      setPassword(ev.target.value);
                      setHasChanges(true);
                    }}
                    required={!!controlPassword}
                  />
                </InputGroup>
                <InputGroup>
                  <FormControl
                    type="password"
                    aria-label="Password"
                    aria-describedby="basic-addon3"
                    placeholder="Enter password again..."
                    value={controlPassword}
                    onChange={(ev) => {
                      setControlPassword(ev.target.value);
                      setHasChanges(true);
                    }}
                    required={!!password}
                  />
                </InputGroup>
              </Row>
              <Row className="p-3">
                <Form.Group controlId="formBasicCheckbox">
                  <Form.Check
                    checked={data.twoFA_is_active || false}
                    onChange={(ev) => {
                      setData({ ...data, twoFA_is_active: ev.target.checked });
                      setHasChanges(true);
                    }
                    }
                    type="checkbox"
                    label="Activate Two-factor Authentication (Login verification by mail)"
                  />
                </Form.Group>
              </Row>
              <Row className="p-3">
                <Button variant="primary" type="submit" disabled={!hasChanges}>
                  Save Changes
                </Button>
              </Row>
            </Form>
          ) : (
            <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">Email</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                aria-label="Email"
                aria-describedby="basic-addon1"
                readOnly={true}
                value={data.user_mail}
              />
            </InputGroup>
          )}
        </Col>
      </Row>
      <Row className="justify-content-center pt-3">
        <h2>Request History</h2>
      </Row>
      <Row className="justify-content-center">
        <Table striped bordered hover className="w-auto">
          <thead>
            <tr>
              <th>Title</th>
              <th>Start</th>
              <th>End</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {
              [...requests]
            .sort((a,b) => 
              (a.period_start > b.period_start) ? 1: -1)
             .map((request, index) => (
              <RequestHistoryItem key={index} request={request} />
            ))}
          </tbody>
        </Table>
      </Row>
      <div>
        <br />
        <Button type="button" onClick={() => goBackHistory.goBack()}>
          Back
        </Button>
      </div>
    </Container>
  );
};

export default ViewUser;
