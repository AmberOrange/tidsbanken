import React from "react";

const UserListItem = ( {click, user: {profile_pic, first_name, last_name, user_mail, twoFA_is_active, is_admin}}) => {
  return (
    <tr onClick={click} style={{ cursor: "pointer" }}>
      <td>
        <img
          src={profile_pic}
          alt="Profile"
          width="64px"
        />
      </td>
      <td className="text-center align-middle">{first_name}</td>
      <td className="text-center align-middle">{last_name}</td>
      <td className="text-center align-middle">{user_mail}</td>
      <td className="text-center align-middle">
        <span role="img" aria-label="Two Factor Authentication">
          {twoFA_is_active ? "✔️" : "❌"}
        </span>
      </td>
      <td className="text-center align-middle">
        <span role="img" aria-label="Admin">
          {is_admin ? "✔️" : "❌"}
        </span>
      </td>
    </tr>
  );
};

export default UserListItem;