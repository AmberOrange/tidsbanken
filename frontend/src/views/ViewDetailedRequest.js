import React, { useEffect, useRef, useState } from "react";
import { Button, Table } from "react-bootstrap";
import {
  getSingleRequest,
  setVacationRequestStatus,
  toastEndpoint,
} from "../api/EndPoints";
import moment from "moment";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";

const ViewDetailedRequest = (props) => {
  const [sending, setSending] = useState(false);
  const [data, setData] = useState({
    id: -1,
    title: "",
    period_start: "",
    period_end: "",
    status_id: 2,
    tidsbankenUser: {
      first_name: "",
      last_name: "",
    },
  });
  const history = useRef(useHistory());

  const paramId = useRef(props.match.params.id);

  useEffect(() => {
    toastEndpoint(
      () => getSingleRequest(paramId.current),
      (resp) => setData(resp.data),
      () => history.current.push("/")
    );
  }, []);

  //convert the value of id number to a string
  const statusHandler = (statusID) => {
    switch (statusID) {
      case 0:
        return "Pending";
      case 1:
        return "Approved";
      case -1:
        return "Denied";
      default:
        return "Unknown";
    }
  };

  //Redirect the user back to calender
  const handleClick = () => {
    history.current.push("/calander");
  };

  const handleStatus = (status) => {
    setSending(true);
    toastEndpoint(
      () => setVacationRequestStatus(data.id, status),
      (resp) => {
        setData({ ...data, status_id: resp.data });
        toast.success("Successfully updated status");
        setSending(false);
      },
      (err) => {
        setSending(false);
      }
    );
  };

  return (
    <div align="center">
      <h1>Vacation request overview!</h1>
      <div align="center">
        <Table striped bordered hover>
          <thead align="center">
            <tr>
              <th>
                {data.tidsbankenUser.first_name} {data.tidsbankenUser.last_name}{" "}
              </th>
            </tr>
          </thead>
          <tbody align="center">
            <tr>
              <td>Title: {data.title}</td>
            </tr>
            <tr>
              <td>From: {moment(data.period_start).format("YYYY-MM-DD")}</td>
            </tr>
            <tr>
              <td>To: {moment(data.period_end).format("YYYY-MM-DD")}</td>
            </tr>
            <tr>
              <td>Status: {statusHandler(data.status_id)}</td>
            </tr>
          </tbody>
        </Table>
        {/* if the statusid is 0 (pending), the button approve and deny will show */}
        {data.status_id === 0 && (
          <React.Fragment>
            <Button
              variant="success"
              onClick={() => {
                handleStatus(1);
              }}
              disabled={sending}
            >
              Approve request
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                handleStatus(-1);
              }}
              disabled={sending}
            >
              Deny request
            </Button>
            <br />
          </React.Fragment>
        )}
        <br />
        <button type="button" disabled={sending} onClick={handleClick}>
          Back to calender
        </button>
        {sending && (
          <React.Fragment>
            <br />
            <label>Sending request...</label>
          </React.Fragment>
        )}
      </div>
    </div>
  );
};
export default ViewDetailedRequest;
