import axios from "axios";
import { toast } from "react-toastify";
import { BASE_URL } from "../environment/environment";
import errorHandler from "./errorHandler";

/**
 * Calls an endpoint function and returns a promise.
 *
 * @param {function} endpoint
 * @returns {Promise} Returns a promise.
 */
export const promiseEndpoint = (endpoint) =>
  new Promise((resolve, reject) => {
    try {
      resolve(endpoint());
    } catch (e) {
      reject(e);
    }
  });

/**
 * Calls an endpoint function, callback on success, toast on failure.
 *
 * This function will show a toast error message on any caught
 * errors for simpler error handling.
 *
 * @param {function} endpoint
 * @param {function} successCallback
 */
export const toastEndpoint = (
  endpoint,
  successCallback,
  failedCallback = null
) =>
  promiseEndpoint(endpoint)
    .then((resp) => successCallback(resp))
    .catch((err) => {
      toast.error(err);
      if (failedCallback !== null) failedCallback(err);
    });

/**
 * GET-request on '/' for testing purposes.
 * s
 * @returns {object} Returns a common response.
 */
export const getMainPoint = () => {
  return axios
    .get(`${BASE_URL}/`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * GET-request on '/request/:id' to get a single request.
 *
 * @returns {object} Returns a common response.
 */
export const getSingleRequest = (id) => {
  return axios
    .get(`${BASE_URL}/request/${id}`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * GET-request on '/request' to get requests within a date range.
 *
 * @returns {object} Returns a common response.
 */
export const getRequestsWithinDateRange = (from, to) => {
  return axios
    .get(`${BASE_URL}/request?startDate=${from}&endDate=${to}`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * POST-request on '/request' to send a vacation request.
 *
 * @returns {object} Returns a common response.
 */
export const postNewVacationRequest = (data) => {
  return axios
    .post(`${BASE_URL}/request`, data)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * 
 * PATCH-request on '/request' to set new dataobj with updated title or/and dates 
 * @return {object} Returns common reponse  
 */

export const setUpdatedVacationRequest = (id, data) => {
  return axios
    .patch(`${BASE_URL}/request/${id}`,data)
    .then((resp) => resp.data)
    .catch(errorHandler);
};


/**
 * PATCH-request on '/set-status' to set status of a request.
 *
 * @returns {object} Returns a common response.
 */
export const setVacationRequestStatus = (id, status) => {
  return axios
    .patch(`${BASE_URL}/set-status/${id}/${status}`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * GET-request on '/request/:id' to get a single request.
 *
 * @returns {object} Returns a common response.
 */
export const getSingleUser = (id) => {
  return axios
    .get(`${BASE_URL}/user/${id}`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * GET-request on '/user-requests/:id' to get user's requests.
 * If the current authenitcated user is now allowed to see pending
 * requests, exclude those.
 *
 * @returns {object} Returns a common response.
 */
export const getUsersRequests = (id) => {
  return axios
    .get(`${BASE_URL}/user-requests/${id}`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * GET-request on '/users' to get all users.
 * 
 * @returns {object} Returns a common response.
 */
export const getAllUsers = () => {
  return axios
    .get(`${BASE_URL}/users`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};


 /* PATCH-request on '/users/:id' to partially patch an user
 /* 
 /* @returns {object} Returns a common response.
 /*/
export const patchUser = (id, data) => {
  return axios
    .patch(`${BASE_URL}/user/${id}`, data)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

 /* POST-request on '/login' to login
 /*
 /* @returns {object} Returns a common response.
 /*/
export const loginUser = (email, password, code) => {
  return axios
    .post(`${BASE_URL}/login`, {
      user_mail: email,
      user_password: password,
      twoFA_code: code
    })
    .then((resp) => resp.data)
    .catch(errorHandler);
};

 /* POST-request on '/user' to create a new user
 /* 
 /* @returns {object} Returns a common response.
 /*/
 export const postUser = (data) => {
  return axios
    .post(`${BASE_URL}/user`, data)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * GET-request on '/user to get current user using JWT token
 * 
 * @returns {object} Returns a common response.
 */
export const getCurrentUser = () => {
  return axios
    .get(`${BASE_URL}/user/`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * POST-request on '/request/{id}/comment' to send a vacation request.
 *
 * @returns {object} Returns a common response.
 */
export const postNewComment = (id, data) => {
  return axios
    .post(`${BASE_URL}/request/${id}/comment`, data)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

 /* POST-request on '/ineligible' to post an ineligible period
 /*
 /* @returns {object} Returns a common response.
 /*/
 export const postIneligiblePeriod = (from, to) => {
  return axios
    .post(`${BASE_URL}/ineligible`, {
      period_start: from,
      period_end: to,
    })
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * GET-request on '/ineligible' to get all ineligible periods between two dates
 * 
 * @returns {object} Returns a common response.
 */
export const getIneligiblePeriodWithinDateRange = (from, to) => {
  return axios
    .get(`${BASE_URL}/ineligible?startDate=${from}&endDate=${to}`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

/**
 * GET-request on '/ineligible' to get all ineligible periods between two dates
 * 
 * @returns {object} Returns a common response.
 */
export const getIneligiblePeriods = () => {
  return axios
    .get(`${BASE_URL}/ineligible`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

export const deleteRequest = (id) => {
  return axios
  .delete(`${BASE_URL}/request/${id}`)
  .then((resp) => resp.data)
  .catch(errorHandler);
};

  /**
 * GET-request on '/notifications' to get notification items relevant to the current
 * logged in user
 * 
 * @returns {object} Returns a common response.
 */
export const getNotifications = () => {
  return axios
    .get(`${BASE_URL}/notifications`)
    .then((resp) => resp.data)
    .catch(errorHandler);
};

export const getAllRequests = () => {
  return axios
  .get(`${BASE_URL}/request`)
  .then((resp) => resp.data)
  .catch(errorHandler);
};

/**
 * POST-request on '/user/:id/update_password' to update a specific users password
 *
 * @returns {object} Returns a common response.
 */
export const postNewUserPassword = (id, data) => {
  return axios
    .post(`${BASE_URL}/user/${id}/update_password`, data)
    .then((resp) => resp.data)
    .catch(errorHandler);
};