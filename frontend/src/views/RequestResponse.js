import React, { useState } from "react";
import { Button, Table } from "react-bootstrap";

const RequestResponse = () => {
  //TODO: (User story) get values from backend and patch the statusID for the
  //specific vacation request
  const [request, setRequest] = useState({
    requests: {
      requestId: 1,
      title: "After work",
      period_start: "2020-09-12T18:44:52.134+00:00",
      period_end: "2020-09-20T18:44:52.134+00:00",
      time_created: "2020-08-02T18:44:52.134+00:00",
      owner_id: 1,
      status_id: 0,
    },
    users: {
      id: 1,
      first_name: "Göken",
      last_name: "Olsson",
      user_mail: "goken@mailapp.com",
      user_password: "abc",
      profile_pic: "profile/pic2.jpg",
      is_admin: true,
      latest_active: null,
    },
  });

  const [statusMessage, setStatusMessage] = useState("Pending");

  //store the requests object in "id" variable for handeling the accept button
  const handleAccept = (id) => {
    if (id) {
      //if id keeps a value, using prevState as an updater.
      //had to place id.status_is=1 outside of setRequest for som reason???
      setRequest((prevState) => ({
        ...prevState,
      }));
      id.status_id = 1;
      setStatusMessage("Accepted");
    }
    console.log(id);
  };
  //handeling the reject button. same as above. Console log to see the fiinal object
  const handleReject = (id) => {
    if (id) {
      setRequest((prevState) => ({
        ...prevState,
      }));
      id.status_id = -1;
      setStatusMessage("Rejected");
    }
    console.log(id);
  };

  //  //convert the date to year, month and day
  const handleDateFormat = (date) => {
    let currentDate = new Date(date);
    let formatted_date =
      currentDate.getFullYear() +
      "-" +
      (currentDate.getMonth() + 1) +
      "-" +
      currentDate.getDate();
    return formatted_date;
  };

  return (
    <div align="center">
      <div>
        <Table striped bordered hover>
          <thead align="center">
            <tr>
              <th>
                {" "}
                {request.users.first_name} {request.users.last_name}{" "}
              </th>
            </tr>
          </thead>
          <tbody align="center">
            <tr>
              <td>
                Title: <label>{request.requests.title}</label>
              </td>
            </tr>
            <tr>
              <td>From: {handleDateFormat(request.requests.period_start)}</td>
            </tr>
            <tr>
              <td>To: {handleDateFormat(request.requests.period_end)}</td>
            </tr>
            <tr>
              <td>
                Comments: <label></label>
              </td>
            </tr>
            <tr>
              <td>
                Status:
                {/* if status_is is -1 or 1. the updated statusmessage will show */}
                {!request.requests.status_id === 0 ||
                  ((-1 || 1) && (
                    <React.Fragment>
                      <label>{statusMessage}</label>
                    </React.Fragment>
                  ))}
              </td>
            </tr>
            <tr></tr>
          </tbody>
        </Table>
      </div>{" "}
      <br />
      {/* if the statusid is 0 (pending), the button accept and reject will show */}
      {request.requests.status_id === 0 && (
        <React.Fragment>
          <Button
            variant="success"
            onClick={() => {
              handleAccept(request.requests);
            }}
          >
            Accept request
          </Button>
          <Button
            variant="danger"
            onClick={() => {
              handleReject(request.requests);
            }}
          >
            Reject request
          </Button>
        </React.Fragment>
      )}
    </div>
  );
};
export default RequestResponse;
