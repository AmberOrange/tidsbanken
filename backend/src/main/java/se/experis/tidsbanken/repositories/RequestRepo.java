package se.experis.tidsbanken.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import se.experis.tidsbanken.models.VacationRequest;

import java.util.Calendar;
import java.util.List;

/**
 * This class converts VacationRequest objects into database records and vice versa.
 * Also contains custom queries for special operations needed in the program
 *
 * @author - Karl Löfquist, Emil Grenebrant and Tora Haukka
 */
public interface RequestRepo extends JpaRepository<VacationRequest, Integer> {
    /**
     * Get a specific VacationRequest by id
     *
     * @param id - Id of VacationRequest
     * @return - specific VacationRequest object
     */
    VacationRequest getById(Integer id);

    /**
     * Custom query for getting requests with period_start and period_end within client specified dates
     *
     * @param start - client specified start date
     * @param end - client specified end date
     * @return - List of found vacation requests
     */
    @Query("SELECT r FROM VacationRequest r WHERE r.period_end >= ?1 and r.period_start <= ?2")
    List<VacationRequest> findRequestsBetweenDates(Calendar start, Calendar end);

    /**
     * Custom query for getting requests within period_start and period_end that the user is allowed to view
     *
     * @param start - client specified start date
     * @param end - client specified end date
     * @param id - id from current logged in user
     * @return - List of found vacation requests
     */
    @Query("SELECT r FROM VacationRequest r WHERE r.period_end >= ?1 and r.period_start <= ?2 and (r.tidsbankenUser.id = ?3 or r.status_id = 1)")
    List<VacationRequest> findRequestsBetweenDatesAndUser(Calendar start, Calendar end, int id);

    /**
     * Custom query for getting all requests that the user is allowed to view
     *
     * @param id - id from current logged in user
     * @return - List of found vacation requests
     */
    @Query("SELECT r FROM VacationRequest r WHERE r.tidsbankenUser.id = ?1 or r.status_id = 1")
    List<VacationRequest> findAllRequestsUser(int id);

    /**
     * Custom query for updating specified (by id) vacation request's time_updated
     *
     * @param id - client specified id of vacation request
     * @param timeUpdated - time (now) to be set as time_updated in update of vacation request
     * @return - Number of updated rows (1 if success, otherwise 0)
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update VacationRequest r set r.time_updated = :timeUpdated where r.id = :id")
    int updateTimeUpdated(@Param("id") Integer id, @Param("timeUpdated") Calendar timeUpdated);

    /**
     * Custom query for getting requests from a user that is not pending
     *
     * @param id - The id of the user
     * @return - List of found vacation requests
     */
    @Query("SELECT r FROM VacationRequest r WHERE r.tidsbankenUser.id = ?1 and r.status_id = 1")
    List<VacationRequest> getNonPendingRequestsFromUser(int id);

    /**
     * Custom query for getting requests created at specified time.
     *
     * @param calendar - client specified time
     * @return - List of found vacation requests
     */
    @Query("SELECT r FROM VacationRequest r WHERE r.time_created >= ?1")
    List<VacationRequest> findRequestsCreatedSinceDate(Calendar calendar);

    /**
     * Custom query for getting requests updated at specified time.
     *
     * @param calendar - client specified time
     * @return - List of found vacation requests
     */
    @Query("SELECT r FROM VacationRequest r WHERE r.time_updated >= ?1")
    List<VacationRequest> findRequestsUpdatedSinceDate(Calendar calendar);

    /**
     * Custom query for getting requests updated at specified time by specified user.
     *
     * @param calendar - client specified time
     * @param id - Integer - user_id provided by client
     * @return - List of found vacation requests
     */
    @Query("SELECT r FROM VacationRequest r WHERE r.time_updated >= ?1 and r.tidsbankenUser.id = ?2")
    List<VacationRequest> findUserRequestsUpdatedSinceDate(Calendar calendar, int id);
}
