import React from "react";
import { Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import moment from "moment";

const NotificationItem = ({ thumbnail, message, date, link, latest }) => {
  return (
    <React.Fragment>
      <hr />
      <Row className="ml-2 mr-2">
        <Col xs="auto">
          <img src={thumbnail} alt="Profile" width="30px" />
        </Col>
        <Col>
          <Row>{latest < date && "New:" } {message}</Row>
          <Row>
            <Col>
              <small className="text-left">{moment(date).fromNow()}</small>
            </Col>
            <Col>
              <small className="text-right">
                <Link to={link}>View the event</Link>
              </small>
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default NotificationItem;
