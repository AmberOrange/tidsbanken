package se.experis.tidsbanken.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Date;

/**
 * Class for handling JWT operations
 *
 * @author - Emil Grenebrant
 */
public class JwtTool {

    private static final Algorithm ALGORITHM_HS = Algorithm.HMAC256("secretsauce");
    private static final JWTVerifier VERIFIER = JWT.require(ALGORITHM_HS)
            .withIssuer("auth0")
            .build();

    /**
     * Method for creating a JWT token
     *
     * @param id - Id of a TidsbankenUser to be set in JWT
     * @return - Created JWT token
     */
    public static String createToken(int id) {
        String token = null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR,24);

        try {
            token = JWT.create()
                    .withIssuer("auth0")
                    .withSubject(Integer.toString(id))
                    .withExpiresAt(cal.getTime())
                    .sign(ALGORITHM_HS);
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
            System.out.println("Invalid Signing configuration / Couldn't convert Claims.");
            System.out.println(exception.getMessage());
        }
        return token;
    }

    /**
     * Method for decoding a JWT token
     *
     * @param token - JWT token to be decoded
     * @return - Decoded JWT token
     */
    public static DecodedJWT decodeToken(String token) {
        DecodedJWT jwt = null;
        try {
            jwt = VERIFIER.verify(token);
        } catch (JWTVerificationException exception){
            //Invalid signature/claims
            System.out.println("Invalid signature/claims");
        } catch (Exception exception) {
            System.out.println("An exception occurred trying to decode the JWT");
            System.out.println(exception.getMessage());
        }
        return jwt;
    }
}
