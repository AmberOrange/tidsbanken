const { SET_USER, DELETE_USER } = require("../actions/action.types");

const initialState = {
  id: -1,
  first_name: "",
  last_name: "",
  user_mail: "",
  profile_pic: "",
  is_admin: false,
  latest_active: 0,
};

const userReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_USER:
      return action.user;
    case DELETE_USER:
      return initialState;
    default:
      return state;
  }
};

export default userReducer;
