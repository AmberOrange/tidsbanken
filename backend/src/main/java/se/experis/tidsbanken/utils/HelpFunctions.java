package se.experis.tidsbanken.utils;

import se.experis.tidsbanken.models.Comment;
import se.experis.tidsbanken.models.IneligiblePeriod;
import se.experis.tidsbanken.models.TidsbankenUser;
import se.experis.tidsbanken.models.VacationRequest;

import java.util.Calendar;
import java.util.List;

/**
 * Util class HelpFunctions which contains functions for operations needed in the program
 */
public class HelpFunctions {

    /**
     * Method for checking so that no vital request information is missing.
     *
     * @param request - VacationRequest object containing information (sent by client) to be checked.
     * @return - True if valid, else false.
     */
    public static boolean validateRequest(VacationRequest request) {
        return (notNullOrEmpty(request.title)
                && request.period_start != null
                && request.period_end != null
                && request.time_created != null
                && (request.status_id >= -1
                && request.status_id <= 1));
    }

    /**
     * Method for checking so that no vital user information is missing.
     *
     * @param user - User object containing information (sent by client) to be checked.
     * @return - True if valid, else false.
     */
    public static boolean validateUser(TidsbankenUser user) {
        return (notNullOrEmpty(user.first_name) &&
                notNullOrEmpty(user.last_name) &&
                notNullOrEmpty(user.user_mail) &&
                notNullOrEmpty(user.user_password) &&
                user.is_admin != null);
    }

    /**
     * Method for checking so that no vital ineligible period information is missing.
     *
     * @param ineligiblePeriod - IneligiblePeriod object containing information (sent by client) to be checked.
     * @return - True if valid, else false.
     */
    public static boolean validateIneligiblePeriod(IneligiblePeriod ineligiblePeriod) {
        return (ineligiblePeriod.period_start != null &&
                ineligiblePeriod.period_end != null &&
                ineligiblePeriod.admin_id > 0);
    }

    /**
     * Method for checking so that no vital comment information is missing.
     *
     * @param comment - Comment object containing information (sent by client) to be checked.
     * @return - True if valid, else false.
     */
    public static boolean validateComment(Comment comment) {
        return (notNullOrEmpty(comment.message)
                && comment.vacationRequest != null
                && comment.tidsbankenUser != null);
    }

    /**
     * Method for trimming a Calendar object. This function sets a specific Calendar objects time data to 0.
     * This is done because of later comparison between this date the client specified dates (which also has time data set to 0)
     *
     * @param calendar - Calendar object to be trimmed to remove time data
     */
    public static void trimCalendar(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    /**
     * Method that checks whether two Calendar objects fall on the same date
     *
     * @param first - First Calendar object
     * @param second - Second Calendar object
     * @return - True if the two calendars are the same date, else false
     */
    public static boolean isCalendarSameDate(Calendar first, Calendar second) {
        return first.get(Calendar.YEAR) == second.get(Calendar.YEAR)
                && first.get(Calendar.MONTH) == second.get(Calendar.MONTH)
                && first.get(Calendar.DAY_OF_MONTH) == second.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Outsource method to check if the comment is editable (if it has not been more than 24 hours since it was created.
     *
     * @param comment - comment to be validated
     * @return - returns true if the comment is editable
     */
    public static boolean editableComment(Comment comment) {
        Calendar rightNow = Calendar.getInstance();
        Calendar expireTime = comment.time_created;
        expireTime.add(Calendar.HOUR, 24);
        return rightNow.compareTo(expireTime) < 0;
    }

    /**
     * Outsource-method that checks each input string for not null or empty.
     *
     * @param s - String to be validated
     * @return - True if valid (not null or empty), otherwise false
     */
    private static boolean notNullOrEmpty(String s) {
        return s != null && !s.equals("");
    }

}
