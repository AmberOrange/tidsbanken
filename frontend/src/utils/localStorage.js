
export const setStorage = (key, value) => { 
    //set the key and value into localstorage
    localStorage.setItem(key, value);
};

export const getStorage = (key) => { 
    //get the key from the loalStorage    
    const storedValue = localStorage.getItem(key);
    //if the storedValue is false (null) , return false. Else true
    if(!storedValue) return false;
    return storedValue;
};

export const deleteStorage = () => {
    localStorage.removeItem('token');
};
