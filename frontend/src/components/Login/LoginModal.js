import React, { useState } from "react";
import {
  Button,
  Form,
  FormControl,
  InputGroup,
  Modal,
  Row,
} from "react-bootstrap";
import styles from "./LoginModal.module.css";

const LoginModal = ({ show, onHide, onSubmit }) => {
  const [isProcessing, setIsProcessing] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [validated, setValidated] = useState(false);
  const [code, setCode] = useState();

  const handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      setValidated(true);
    } else {
      setIsProcessing(true);
      onSubmit(code);
      setIsProcessing(false);
    }
  };

  return (
    <Modal
      show={show}
      onShow={() => setErrorMessage("")}
      onHide={onHide}
      dialogClassName={styles.modalWidth}
      aria-labelledby="contained-modal-title-vcenter"
      animation={false}
      backdrop={"static"}
      centered
    >
      <Modal.Header closeButton={false}>
        <Modal.Title id="contained-modal-title-vcenter">
          Two-factor Authentication
        </Modal.Title>
      </Modal.Header>
      <Form validated={validated} onSubmit={handleSubmit} noValidate>
        <Modal.Body className="m-3">
          <Row className="justify-content-center">
            A mail containing a verification code has been sent to your email.
            <br />
            Please verify by entering the code below:
          </Row>
          <Row className="justify-content-center p-4">
            <InputGroup className="mb-3" style={{ maxWidth: "10em" }}>
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">Code</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="1234..."
                aria-label="Vertification"
                aria-describedby="basic-addon1"
                value={code || ""}
                onChange={(ev) => {
                  setValidated(false);
                  setCode(ev.target.value);
                }}
                minLength={4}
                maxLength={4}
                required
              />
              <FormControl.Feedback type="invalid">
                Please enter a 4-digit code.
              </FormControl.Feedback>
            </InputGroup>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <i>
            <small style={{ color: "red" }}>{errorMessage}</small>
          </i>
          <Button variant="primary" type="submit" disabled={isProcessing}>
            Submit
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default LoginModal;
