package se.experis.tidsbanken.controllers;

import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import se.experis.tidsbanken.models.CommonResponse;
import se.experis.tidsbanken.models.TidsbankenUser;
import se.experis.tidsbanken.repositories.UserRepo;
import se.experis.tidsbanken.utils.CommonException;
import se.experis.tidsbanken.utils.JwtTool;

/**
 * Token controller. This controller performs user- and admin operations for user verification in endpoints
 *
 * @author Emil Grenebrant
 * @version 0.1.0
 */
@RestController
public class TokenController {
    private final UserRepo userRepo;

    public TokenController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    /**
     * Method that checks supplied user token if the user is the author of an object or has administration privileges
     *
     * @param token - JWT token for verification
     * @param id - Id of object to be checked
     * @return - Verified user
     * @throws CommonException - Custom exception that throws http response errors
     */
    public TidsbankenUser getAuthorOrAdminFromToken(String token, int id) throws CommonException {
        TidsbankenUser user = getUserFromToken(token);
        if(!user.is_admin && user.id != id) {
            CommonResponse cr = new CommonResponse();
            cr.data = null;
            cr.message = "Only an admin or the author has access";

            throw new CommonException(new ResponseEntity<>(cr, HttpStatus.FORBIDDEN));
        }
        return user;
    }

    /**
     * Method that checks if user (specified by token) is admin or not
     *
     * @param token - JWT token for verification
     * @return - User who has administration privileges
     * @throws CommonException - Custom exception that throws http response errors
     */
    public TidsbankenUser getAdminFromToken(String token) throws CommonException {
        TidsbankenUser user = getUserFromToken(token);
        if(!user.is_admin) {
            CommonResponse cr = new CommonResponse();
            cr.data = null;
            cr.message = "Only an admin has access";

            throw new CommonException(new ResponseEntity<>(cr, HttpStatus.FORBIDDEN));
        }
        return user;
    }

    /**
     * Method that returns the complete TidsbankenUser from id specified in JWT token
     *
     * @param token - JWT token for verification
     * @return - Complete TidsbankenUser object from id specified in token
     * @throws CommonException - Custom exception that throws http response errors
     */
    public TidsbankenUser getUserFromToken(String token) throws CommonException {
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus resp;
        DecodedJWT jwt;
        TidsbankenUser user;

        if(!token.isEmpty()) {
            if((jwt = JwtTool.decodeToken(token)) != null) {
                int id = Integer.parseInt(jwt.getSubject());
                if(userRepo.existsById(id)) {
                    user = userRepo.getById(id);
                } else {
                    cr.message = "The user does not exist";
                    resp = HttpStatus.NOT_FOUND;
                    throw new CommonException(new ResponseEntity<>(cr, resp));
                }
            } else {
                cr.message = "Token not valid";
                resp = HttpStatus.UNAUTHORIZED;
                throw new CommonException(new ResponseEntity<>(cr, resp));
            }
        } else {
            cr.message = "No token provided in Authorization header";
            resp = HttpStatus.UNAUTHORIZED;
            throw new CommonException(new ResponseEntity<>(cr, resp));
        }
        return user;
    }
}
