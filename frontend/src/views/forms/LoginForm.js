import axios from "axios";
import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { getCurrentUser, loginUser } from "../../api/EndPoints";
import LoginModal from "../../components/Login/LoginModal";
import { setUserAction } from "../../store/actions/user.actions";
import { setStorage } from "../../utils/localStorage";

const LoginForm = () => {
  const [userName, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [loginError, setLoginError] = useState("");

  const [showModal, setShowModal] = useState(false);

  const dispatch = useDispatch();

  const handleSubmit = async (ev) => {
    ev.preventDefault();

    setLoginError("");

    //try the loginUser from Endpoint with the input value of username
    handleLogin(null);
  };

  const handleLogin = async (code) => {
    try {
      let loginResult = await loginUser(userName, password, code);

      if(loginResult.data.twoFA) {
        setShowModal(true);
      }

      else {
        setStorage("token", loginResult.data);
        axios.defaults.headers.common["Authorization"] = loginResult.data;
  
        let { data } = await getCurrentUser();
        dispatch(
          setUserAction({
            id: data.id,
            first_name: data.first_name,
            last_name: data.last_name,
            user_mail: data.user_mail,
            profile_pic: data.profile_pic,
            is_admin: data.is_admin,
            latest_active: data.latest_active,
          })
        );
      }

    } catch (e) {
      toast.error(e.message || e);
    }
  }

  const on2faCodeSubmit = (code) => {
    handleLogin(code);
  };

  return (
    <Container>
      <LoginModal show={showModal} onHide={() => setShowModal(false)} onSubmit={on2faCodeSubmit} />
      <h1>Login</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Email </label>
          <input
            type="email"
            className="form-control"
            placeholder="Enter email..."
            onChange={(ev) => {
              setUsername(ev.target.value.trim());
            }}
          />
        </div>
        <div className="form-group">
          <label>Password </label>
          <input
            type="password"
            className="form-control"
            placeholder="Password..."
            onChange={(ev) => {
              setPassword(ev.target.value.trim());
            }}
          ></input>
        </div>
        <button type="submit" className="btn btn-primary btn-block">
          Login
        </button>
        <p className="forgot-password">Forgot password?</p>
        {loginError && <p> {loginError} </p>}
      </form>
    </Container>
  );
};
export default LoginForm;
