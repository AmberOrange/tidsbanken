package se.experis.tidsbanken.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import se.experis.tidsbanken.models.TidsbankenUser;

import java.util.Calendar;

/**
 * This class converts TidsbankenUser objects into database records and vice versa.
 * Also contains custom queries for special operations needed in the program
 *
 * @author - Karl Löfquist
 */
public interface UserRepo extends JpaRepository<TidsbankenUser, Integer> {
    /**
     * Get a specific TidsbankenUser by id
     *
     * @param id - Id of TidsbankenUser
     * @return - specific TidsbankenUser object
     */
    TidsbankenUser getById(Integer id);

    /**
     * Custom query for updating specified (by id) TidsbankenUser
     *
     * @param id - Integer - id of user to be updated
     * @param first_name - String - new first_name to be updated in database
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update TidsbankenUser u set u.first_name = :first_name where u.id = :id")
    int updateUserFirstName(@Param("id") Integer id, @Param("first_name") String first_name);

    /**
     * Custom query for updating specified (by id) TidsbankenUser
     *
     * @param id - Integer - id of user to be updated
     * @param last_name - String - new last_name to be updated in database
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update TidsbankenUser u set u.last_name = :last_name where u.id = :id")
    int updateUserLastName(@Param("id") Integer id, @Param("last_name") String last_name);

    /**
     * Custom query for updating specified (by id) TidsbankenUser
     *
     * @param id - Integer - id of user to be updated
     * @param user_mail - String - new user_mail to be updated in database
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update TidsbankenUser u set u.user_mail = :user_mail where u.id = :id")
    int updateUserMail(@Param("id") Integer id, @Param("user_mail") String user_mail);

    /**
     * Custom query for updating specified (by id) TidsbankenUser
     *
     * @param id - Integer - id of user to be updated
     * @param profile_pic - String - new profile_pic url to be updated in database
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update TidsbankenUser u set u.profile_pic = :profile_pic where u.id = :id")
    int updateUserProfilePic(@Param("id") Integer id, @Param("profile_pic") String profile_pic);

    /**
     * Custom query for updating specified (by id) TidsbankenUser
     *
     * @param id - Integer - id of user to be updated
     * @param is_admin - boolean - new is_admin to be updated in database
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update TidsbankenUser u set u.is_admin = :is_admin where u.id = :id")
    int updateUserIsAdmin(@Param("id") Integer id, @Param("is_admin") boolean is_admin);

    /**
     * Custom query for updating specified (by id) TidsbankenUser
     *
     * @param id - Integer - id of user to be updated
     * @param latest_active - Calendar - new latest_active to be updated in database
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update TidsbankenUser u set u.latest_active = :latest_active where u.id = :id")
    int updateUserLatestActive(@Param("id") Integer id, @Param("latest_active") Calendar latest_active);

    /**
     * Custom query for updating whether the TidsbankenUser should use 2fa (by id)
     *
     * @param id - Integer - id of user to be updated
     * @param twoFA_is_active - Boolean - true if the user uses 2fa
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update TidsbankenUser u set u.twoFA_is_active = :twoFA_is_active where u.id = :id")
    int updateUserTwoFactor(@Param("id") Integer id, @Param("twoFA_is_active") Boolean twoFA_is_active);

    /**
     * Custom query for finding a user by email for login verification
     *
     * @param user_mail - String - user_mail provided by client
     * @return - Found TidsbankenUser object
     */
    @Query("SELECT u FROM TidsbankenUser u WHERE u.user_mail = ?1")
    TidsbankenUser findUserWithMail(String user_mail);

    /**
     * Custom query for updating a user's password
     *
     * @param id - Integer - id of user to be updated
     * @param user_password - String - new password to be updated in database
     * @return - int that indicates how many rows have been updated
     */
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update TidsbankenUser u set u.user_password = :user_password where u.id = :id")
    int updateUserPassword(@Param("id") Integer id, @Param("user_password") String user_password);
}
