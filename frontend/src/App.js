import React, { useEffect, useState } from "react";
import { Container, Row } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import AddRequest from "./views/AddRequest";
import ViewDetailedRequest from "./views/ViewDetailedRequest";
import Navheader from "./components/Navheader";
import ToastDisplay from "./components/ToastDisplay";
import Dashboard from "./views/Dashboard";
import Admin from "./views/Admin";
import TestRequests from "./views/TestRequests";
import ViewUser from "./views/ViewUser";
import LoginPage from "./views/LoginPage";
import RequestResponse from "./views/RequestResponse";
import { getCurrentUser, promiseEndpoint } from "./api/EndPoints";
import axios from "axios";
import { deleteStorage, getStorage } from "./utils/localStorage";
import { setUserAction } from "./store/actions/user.actions";
import { useDispatch, useSelector } from "react-redux";
import EditRequestByUser from "./views/EditRequestByUser";

const App = () => {
  const [isSettingUp, setIsSettingUp] = useState(true);

  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    document.title = "Tidsbanken";
    if (getStorage("token")) {
      axios.defaults.headers.common["Authorization"] = getStorage("token");

      promiseEndpoint(getCurrentUser)
        .then(({ data }) => {
          dispatch(
            setUserAction({
              id: data.id,
              first_name: data.first_name,
              last_name: data.last_name,
              user_mail: data.user_mail,
              profile_pic: data.profile_pic,
              is_admin: data.is_admin,
              latest_active: data.latest_active,
            })
          );
          setIsSettingUp(false);
        })
        .catch((err) => {
          delete axios.defaults.headers.common["Authorization"];
          deleteStorage();
          setIsSettingUp(false);
        });
    } else {
      setIsSettingUp(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const GuardedRoute = ({ component: Component, auth, redirect, ...rest }) => (
    <Route {...rest} render={(props) => (
        auth === true
            ? <Component {...props} />
            : user.id !== -1
              ? <Redirect to={"/"} />
              : <Redirect to={"/login"} />
    )} />
)

  return (
    <Container fluid className="vh-100 d-flex flex-column">
      <ToastDisplay />
      {!isSettingUp && (
        <Router>
          {user.id !== -1 && (
            <Row className="flex-column justify-content-center">
              <Navheader />
            </Row>
          )}

          <Switch>
                <GuardedRoute auth={user.id !== -1} exact path="/" component={Dashboard} />
                <GuardedRoute auth={user.id !== -1} path="/addrequest" component={AddRequest} />
                <GuardedRoute auth={user.id !== -1 && user.is_admin} path="/admin" component={Admin} />
                <GuardedRoute
                  auth={user.id !== -1}
                  path="/viewrequest/:id"
                  component={ViewDetailedRequest}
                />
                <GuardedRoute auth={user.id !== -1} path="/viewuser/:id" component={ViewUser} />
                <GuardedRoute auth={user.id !== -1} path="/testrequest" component={TestRequests} />
                <GuardedRoute auth={user.id !== -1} path="/requestresponse" component={RequestResponse} />
                <GuardedRoute auth={user.id !== -1} path="/editrequest/:id" component={EditRequestByUser} />

                <GuardedRoute auth={user.id === -1} path="/login" component={LoginPage} />
                <Route path="*" render={() => ( user.id !== -1 ? <Redirect to="/"/> : <Redirect to="/login"/>)} />
          </Switch>
        </Router>
      )}
    </Container>
  );
};

export default App;
