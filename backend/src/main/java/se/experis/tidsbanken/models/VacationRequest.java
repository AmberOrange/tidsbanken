package se.experis.tidsbanken.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.*;

/**
 * Class VacationRequest which acts as a model for table vacation_request in database
 */
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class VacationRequest implements Comparator<VacationRequest> {

    /**
     * Primary key. Identification of specific instance of VacationRequest
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int id;

    /**
     * Title of VacationRequest
     */
    @Column(nullable = false)
    public String title;

    /**
     * Calendar object that indicates when the vacation is meant to start
     */
    @Column(nullable = false)
    public Calendar period_start;

    /**
     * Calendar object that indicates when the vacation is meant to end
     */
    @Column(nullable = false)
    public Calendar period_end;

    /**
     * Calendar object that indicates when the vacation was created
     */
    @Column(nullable = false)
    public Calendar time_created;

    /**
     * Calendar object that indicates when the vacation was last updated
     */
    @Column
    public Calendar time_updated;

    /**
     * Integer that indicates what status the VacationRequest has (0 = Pending, 1 = Approved and -1 = Denied)
     */
    @Column(nullable = false)
    public int status_id;

    /**
     * ID of admin (only set if an admin has changed something in the VacationRequest)
     */
    @Column
    public Integer admin_id;

    /**
     * Method for getting first_name and last_name of VacationRequest owner
     *
     * @return - Map of key-value pairs (first_name and last_name)
     */
    @JsonGetter("tidsbankenUser")
    public Map<String, Object> getUserInfo() {
        Map<String, Object> temp = new HashMap<>();
        temp.put("id", tidsbankenUser.id);
        temp.put("first_name", tidsbankenUser.first_name);
        temp.put("last_name", tidsbankenUser.last_name);
        return temp;
    }

    /**
     * Many to one relationship between VacationRequest and TidsbankenUser
     */
    @ManyToOne
    @JoinColumn(name = "tidsbankenUser_id")
    public TidsbankenUser tidsbankenUser;

    /**
     * Collection of comments on the vacation request
     */
    @OneToMany(mappedBy = "vacationRequest", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    public Set<Comment> comments = new HashSet<>();

    /**
     * Method that sorts all vacation requests after time_created
     *
     * @param o1 - VacationRequest to be compared to o2
     * @param o2 - VacationRequest to be compared to o1
     * @return - int that indicates whether o1 is going to be placed before or after o2
     */
    @Override
    public int compare(VacationRequest o1, VacationRequest o2) {
        return o2.time_created.compareTo(o1.time_created);
    }

}
