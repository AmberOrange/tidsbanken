package se.experis.tidsbanken.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import se.experis.tidsbanken.models.IneligiblePeriod;

import java.util.Calendar;
import java.util.List;

/**
 * This class converts IneligiblePeriod objects into database records and vice versa.
 * Also contains custom query for special operations needed in the program
 *
 * @author - Karl Löfquist
 */
public interface IneligiblePeriodRepo extends JpaRepository<IneligiblePeriod, Integer> {
    /**
     * Get a specific IneligiblePeriod by id
     *
     * @param id - Id of IneligiblePeriod
     * @return - specific IneligiblePeriod object
     */
    IneligiblePeriod getById(Integer id);

    /**
     * Custom query for getting ineligible periods with period_start and period_end within client specified dates
     *
     * @param start - client specified start date
     * @param end - client specified end date
     * @return - List of found ineligible periods
     */
    @Query("SELECT p FROM IneligiblePeriod p WHERE p.period_end >= ?1 and p.period_start <= ?2")
    List<IneligiblePeriod> findIneligiblePeriodsBetweenDates(Calendar start, Calendar end);

    /**
     * Get a sorted list of ineligible periods sorted by period_start
     *
     * @return - List of ineligible periods sorted chronologically
     */
    @Query("SELECT p FROM IneligiblePeriod p ORDER BY p.period_start ASC")
    List<IneligiblePeriod> sortIPByPeriodStart();
}
