import React, { useState } from "react";
import { Button, Col, Form, InputGroup, Modal, Row } from "react-bootstrap";
import {
  postUser,
  patchUser,
  toastEndpoint,
  postNewUserPassword,
} from "../../api/EndPoints";
import styles from "./UserModal.module.css";

const UserModal = ({
  show,
  onHide,
  modalEditMode,
  userModalInfo,
  setUserModalInfo,
  onRefresh
}) => {
  const fallbackPicture =
    "https://t3.ftcdn.net/jpg/00/64/67/80/240_F_64678017_zUpiZFjj04cnLri7oADnyMH0XBYyQghG.jpg";

  const [picture, setPicture] = useState("");
  const [isProcessing, setIsProcessing] = useState(false);

  const onClickSubmit = (ev) => {
    ev.preventDefault();
    setIsProcessing(true);
    if (modalEditMode) {
      toastEndpoint(
        () => patchUser(userModalInfo.id, userModalInfo),
        () => {
          if (userModalInfo.user_password) {
            toastEndpoint(
              () =>
                postNewUserPassword(userModalInfo.id, {
                  user_password: userModalInfo.user_password,
                }),
              () => {
                onRefresh();
                setIsProcessing(false);
                onHide();
              },
              () => setIsProcessing(false)
            );
          } else {
            onRefresh();
            setIsProcessing(false);
            onHide();
          }
        },
        () => {
          setIsProcessing(false);
        }
      );
    } else {
      toastEndpoint(
        () => postUser(userModalInfo),
        () => {
          onRefresh();
          setIsProcessing(false);
          onHide();
        },
        () => {
          setIsProcessing(false);
        }
      );
    }
  };

  return (
    <Modal
      show={show}
      onShow={() => setPicture(userModalInfo.profile_pic)}
      onHide={onHide}
      dialogClassName={styles.modalWidth}
      aria-labelledby="contained-modal-title-vcenter"
      animation={false}
      backdrop={isProcessing ? "static" : true}
      centered
    >
      <Modal.Header closeButton={!isProcessing}>
        <Modal.Title id="contained-modal-title-vcenter">
          {modalEditMode ? "Edit User" : "Create User"}
        </Modal.Title>
      </Modal.Header>
      <Form onSubmit={onClickSubmit}>
        <Modal.Body>
          <Row>
            <Col xs="auto">
              <img
                className="m-3"
                src={picture}
                alt="Profile"
                width="128px"
                height="128px"
                onError={() => setPicture(fallbackPicture)}
              />
            </Col>
            <Col className="d-flex align-items-center">
              <InputGroup>
                <InputGroup.Prepend>
                  <InputGroup.Text id="basic-addon1">Image Url</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                  placeholder="Paste an image URL here..."
                  aria-label="Username"
                  aria-describedby="basic-addon1"
                  value={userModalInfo.profile_pic || ""}
                  onChange={(e) =>
                    setUserModalInfo({
                      ...userModalInfo,
                      profile_pic: e.target.value,
                    })
                  }
                  onBlur={() => setPicture(userModalInfo.profile_pic)}
                />
              </InputGroup>
            </Col>
          </Row>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text>First/Last name</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control
              type="text"
              placeholder="First name"
              value={userModalInfo.first_name || ""}
              onChange={(e) =>
                setUserModalInfo({
                  ...userModalInfo,
                  first_name: e.target.value,
                })
              }
            />
            <Form.Control
              type="text"
              placeholder="Last name"
              value={userModalInfo.last_name || ""}
              onChange={(e) =>
                setUserModalInfo({
                  ...userModalInfo,
                  last_name: e.target.value,
                })
              }
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="basic-addon1">Email</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control
              placeholder="e.g. john.smith@email.com"
              aria-label="Username"
              aria-describedby="basic-addon1"
              value={userModalInfo.user_mail || ""}
              onChange={(e) =>
                setUserModalInfo({
                  ...userModalInfo,
                  user_mail: e.target.value,
                })
              }
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="basic-addon1">Password</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control
              placeholder="Password"
              aria-label="Password"
              aria-describedby="basic-addon1"
              type="password"
              value={userModalInfo.user_password || ""}
              onChange={(e) =>
                setUserModalInfo({
                  ...userModalInfo,
                  user_password: e.target.value,
                })
              }
            />
          </InputGroup>

          <Form.Group controlId="formBasicCheckbox">
            <Form.Check
              checked={userModalInfo.is_admin || false}
              onChange={(e) =>
                setUserModalInfo({
                  ...userModalInfo,
                  is_admin: e.target.checked,
                })
              }
              type="checkbox"
              label="Give Administration Privileges"
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" type="submit" disabled={isProcessing}>
            Submit
          </Button>
          <Button variant="secondary" onClick={onHide} disabled={isProcessing}>
            Close
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default UserModal;
