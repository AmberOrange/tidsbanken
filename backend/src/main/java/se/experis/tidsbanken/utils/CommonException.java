package se.experis.tidsbanken.utils;

import org.springframework.http.ResponseEntity;
import se.experis.tidsbanken.models.CommonResponse;

/**
 * Custom exception to be thrown when JWT validation fails in some way
 *
 * @author - Emil Grenebrant
 */
public class CommonException extends Exception {
    private final ResponseEntity<CommonResponse> responseEntity;

    public CommonException(ResponseEntity<CommonResponse> responseEntity) {
        this.responseEntity = responseEntity;
    }

    public ResponseEntity<CommonResponse> getResponseEntity() {
        return responseEntity;
    }
}