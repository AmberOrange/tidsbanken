import React from "react";

const NavCustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <div
    style={{ cursor: "pointer" }}
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
  </div>
));

export default NavCustomToggle;
