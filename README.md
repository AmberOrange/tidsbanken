# Tidsbanken

## Contributors
Emil Grenebrant, Tora Haukka, Kalle Löfquist & Martin Sandström

## Live example
https://tidsbankett.herokuapp.com/

See document "Demonstartionspunkter (Demostration notes)" to get a walk through of the application. 

## Pre-requisites
* npm package manager
* Java 14
* Apache Maven 3.6.3
* Postgres

## How to start

The project is split into two parts, a frontend and a backend.
Both parts should be lauched at different ports to communicate with each other.

### Frontend
1. Navigate to the "frontend" directory in the first terminal
2. Run ```npm install``` to install all frontend dependencies
3. (Optional) if the backend is served from another address than ```localhost:8080```:
    * Edit the file at ```frontend\src\environment\environment.js``` and change

        ```export const BASE_URL = "http://localhost:8080";```

        to the desired location
4. Run ```npm start``` to run a development server at ```localhost:3000```

### Backend
1. Navigate to the "backend" directory in a second terminal
3. Run ```mvn spring-boot:run``` to run the endpoint server at ```localhost:8080```

Navigate to ```localhost:3000``` and you will be greeted by a login screen

## Documents

### ERD charts:
https://docs.google.com/document/d/1mxuxUDE_rN7vD06csLhVvH5mLgDlwapJtdHUKDvh4Ck/edit?usp=sharing

### "Uppstart" 21/9 (Initial Planing): 
https://docs.google.com/document/d/1rr5DjcDKJ6vwHgBLeBMir0VYYsI5rGO5pvt94AHFGQk/edit?usp=sharing

### Planering (Plan):
https://docs.google.com/document/d/1R0bPUbJHz3TBR7yaH45lBavL6_qs0MF_1rKWWyEVT8Q/edit?usp=sharing

### Presentationspunkter (Slide notes):
https://docs.google.com/document/d/1NGop0K4BABl3F1-xm6cmh-zop0JzeTVH3u07cbwk7oA/edit?usp=sharing

### Demonstartionspunkter (Demostration notes):
https://docs.google.com/document/d/1ji1Qkycr0g3fOaTDKqgbMvZQ_bZrlECxeDnbEWPHYWI/edit?usp=sharing

### Presentation:
https://docs.google.com/presentation/d/1pVscsndkw91pc3rLiU8nTPJtDIdpsz_6KLI9Sl36x3U/edit?usp=sharing