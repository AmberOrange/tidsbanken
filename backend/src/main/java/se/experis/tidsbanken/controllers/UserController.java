package se.experis.tidsbanken.controllers;

import com.auth0.jwt.interfaces.DecodedJWT;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.tidsbanken.models.CommonResponse;
import se.experis.tidsbanken.models.TidsbankenUser;
import se.experis.tidsbanken.repositories.UserRepo;
import se.experis.tidsbanken.utils.CommonException;
import se.experis.tidsbanken.utils.JwtTool;

import java.util.Calendar;
import java.util.List;

import static se.experis.tidsbanken.utils.HelpFunctions.validateUser;

/**
 * User controller. The controller supplies a number of endpoints related to user handling to be used by the front-end application.
 * The endpoints provides information stored in the database.
 *
 * @author Karl Löfquist, Tora Haukka, Emil Grenebrant and Martin Sandström
 * @version 0.1.0
 */
@RestController
public class UserController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TokenController tokenController;

    /**
     * Method that checks if the user email sent as parameter is unique relative to user emails stored in database
     *
     * @param userMail - User mail address to be checked against all user emails in database
     * @return - Boolean. True if userMail is unique, otherwise false
     */
    private boolean checkUniqueUserMail(String userMail) {

        List<TidsbankenUser> allUsers = userRepo.findAll();

        for (TidsbankenUser user: allUsers) {
            if (user.user_mail.equals(userMail)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Endpoint for getting the current user by verifying the authorization token provided in the header. Uses method GET
     *
     * @param token - JWT token for user verification
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @GetMapping("/user")
    public ResponseEntity<CommonResponse> getCurrentUser(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token) {

        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus resp;

        if (!token.isEmpty()) {
            DecodedJWT jwt = JwtTool.decodeToken(token);

            if (jwt != null) {
                int id = Integer.parseInt(jwt.getSubject());
                if (userRepo.existsById(id)) {
                    cr.data = userRepo.getById(id);
                    cr.message = "User verified and found!";
                    resp = HttpStatus.OK;
                } else {
                    cr.message = "User verified but not found.";
                    resp = HttpStatus.NOT_FOUND;
                }
            } else {
                cr.message = "Invalid token provided.";
                resp = HttpStatus.UNAUTHORIZED;
            }
        } else {
            cr.message = "No Authorization token provided.";
            resp = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Endpoint for getting all users in the database with method GET
     *
     * @param token - JWT token for user verification
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @GetMapping("/users")
    public ResponseEntity<CommonResponse> getAllUsers(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token) {

        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        // Checking the token to see if the user has admin rights
        try {
            tokenController.getAdminFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        //find all objects in the repository
        List<TidsbankenUser> userList = userRepo.findAll();

        if (userList.size() == 0) {
            cr.message = "No users found";
            response = HttpStatus.NOT_FOUND;
        } else {
            cr.message = "Resources fetched";
            cr.data = userList;
            response = HttpStatus.OK;
        }

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for creating a user with method POST
     *
     * @param token - JWT token for user verification
     * @param user - user data in request body
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @PostMapping("/user")
    public ResponseEntity<CommonResponse> createUser(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token, @RequestBody TidsbankenUser user) {

        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        boolean uniqueEmail = checkUniqueUserMail(user.user_mail);

        //Hashing
        String password = user.user_password; //Password entered by admin when creating user
        String hashedPw = BCrypt.hashpw(password, BCrypt.gensalt(10)); //The hashed password that is saved to db

        // Checking the token to see if the user has admin rights
        try {
            tokenController.getAdminFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        //if the entry is empty, return null
        if (!validateUser(user)) {
            cr.message = "Request body not valid";
            resp = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(cr, resp);
        }

        else if (!uniqueEmail) {
            cr.message = "User email address already exists";
            resp = HttpStatus.FORBIDDEN;
        }

        else {
            if (user.twoFA_is_active == null) {
                user.twoFA_is_active = false;
            }
            //process
            user.latest_active = user.recent_login = Calendar.getInstance();
            //saving the hashed version
            user.user_password = hashedPw;
            user = userRepo.save(user);

            cr.data = user;
            cr.message = "New user with id: " + user.id;
            resp = HttpStatus.CREATED;
        }

        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Endpoint for deleting a user with client specified id using method DELETE
     *
     * @param token - JWT token for user verification
     * @param id - id of the user as Integer
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @DeleteMapping("/user/{id}")
    public ResponseEntity<CommonResponse> deleteUser(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token, @PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        HttpStatus resp;


        // Checking the token to see if the user has admin rights
        try {
            tokenController.getAdminFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (userRepo.existsById(id)) {
            userRepo.deleteById(id);
            cr.message = "User with id: " + id + " has been successfully removed!";
            resp = HttpStatus.OK;
        } else {
            cr.message = "User with id: " + id + " was not found.";
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Endpoint for updating a user with method PATCH
     *
     * @param token - JWT token for user verification
     * @param id - Client specified id of user to be updated
     * @param updateObject - object sent from client that contains information to be updated in specified user
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @PatchMapping("/user/{id}")
    public ResponseEntity<CommonResponse> patchUser(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token, @PathVariable Integer id, @RequestBody TidsbankenUser updateObject) {

        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        TidsbankenUser user;
        // Checking the token to see if the user has admin rights
        try {
            user = tokenController.getAuthorOrAdminFromToken(token, id);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        int numberOfUpdatedColumns = 0;

        /*TODO: Simplify if statements. No need for all custom queries*/

        if (id >= 1) {
            if (updateObject.first_name != null && user.is_admin) {
                numberOfUpdatedColumns += userRepo.updateUserFirstName(id, updateObject.first_name);
            }

            if (updateObject.last_name != null && user.is_admin) {
                numberOfUpdatedColumns += userRepo.updateUserLastName(id, updateObject.last_name);
            }

            if (updateObject.user_mail != null) {
                numberOfUpdatedColumns += userRepo.updateUserMail(id, updateObject.user_mail);
            }

            if (updateObject.profile_pic != null) {
                numberOfUpdatedColumns += userRepo.updateUserProfilePic(id, updateObject.profile_pic);
            }

            if (updateObject.is_admin != null && user.is_admin) {
                numberOfUpdatedColumns += userRepo.updateUserIsAdmin(id, updateObject.is_admin);
            }

            if (updateObject.latest_active != null && user.is_admin) {
                numberOfUpdatedColumns += userRepo.updateUserLatestActive(id, updateObject.latest_active);
            }

            if (updateObject.twoFA_is_active != null) {
                numberOfUpdatedColumns += userRepo.updateUserTwoFactor(id, updateObject.twoFA_is_active);
            }

            if (numberOfUpdatedColumns == 0) {

                cr.message = "Patch request not valid";
                response = HttpStatus.BAD_REQUEST;
            } else {
                cr.message = "User updated with id: " + id + ". Number of affected columns: " + numberOfUpdatedColumns;
                cr.data = userRepo.getById(id);
                response = HttpStatus.OK;
            }
        } else {
            cr.message = "No user updated. User id out of bounds";
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Endpoint for getting specific user by id using method GET
     *
     * @param token - JWT token for user verification
     * @param id - Client specified user id of TidsbankenUser object
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @GetMapping("/user/{id}")
    public ResponseEntity<CommonResponse> getUserById(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token, @PathVariable Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        // Checking the token to see if the user is logged in and a valid user
        try {
            tokenController.getUserFromToken(token);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (userRepo.existsById(id)) {
            cr.data = userRepo.findById(id);
            cr.message = "User " + id + " found.";
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "User " + id + " does not exist.";
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, resp);
    }

    /**
     * Endpoint for updating a user's (specified by client) password using method POST
     *
     * @param token - JWT token for user verification
     * @param id - Client specified Id of TidsbankenUser to be updated
     * @param updatePassword - Partial TidsbankenUser object sent from client that contains the password to be updated in specified user.
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @PostMapping("/user/{id}/update_password")
    public ResponseEntity<CommonResponse> postUserPassword(@RequestHeader(value = "Authorization", required = false, defaultValue = "") String token, @PathVariable Integer id, @RequestBody TidsbankenUser updatePassword) {

        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        String password = updatePassword.user_password;


        // Checking the token to see if the user has rights to change password
        try {
            tokenController.getAuthorOrAdminFromToken(token, id);
        } catch (CommonException e) {
            return e.getResponseEntity();
        }

        if (id >= 1 && password != null) {
            String updatedHashedPw = BCrypt.hashpw(password, BCrypt.gensalt(10)); //The hashed password that is saved to db
            int updatedPassword = userRepo.updateUserPassword(id, updatedHashedPw);

            cr.message = "User with id " + id + " updated.";
            cr.data = updatedPassword;
            response = HttpStatus.OK;
        } else if (password == null) {
            cr.message = "Post request not valid";
            response = HttpStatus.BAD_REQUEST;
        } else {
            cr.message = "No user updated. Id " + id + " not found.";
            response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }

    /**
     * A development convenience function that adds an admin to ID 1 if the users are empty
     * 
     * TODO: THIS SHOULD NOT GO INTO THE FINAL PRODUCT!
     *
     * @return - Response entity with response message (and data if ok) and HTTP status
     */
    @PostMapping("/setup-admin")
    public ResponseEntity<CommonResponse> createDefaultAdmin() {

        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        //Hashing
        String password = "abc123";
        String hashedPw = BCrypt.hashpw(password, BCrypt.gensalt(10)); //The hashed password that is saved to db

        if(!userRepo.existsById(1)) {
            TidsbankenUser newUser = new TidsbankenUser();
            newUser.is_admin=true;
            newUser.user_mail="admin@mail.com";
            newUser.user_password=hashedPw;
            newUser.first_name="Admin";
            newUser.last_name="";
            newUser.latest_active= Calendar.getInstance();
            newUser.profile_pic="";
            newUser.twoFA_is_active = false;

            cr.data = userRepo.save(newUser);
            cr.message = "Default admin created";

            response = HttpStatus.CREATED;
        } else {
            cr.data = null;
            cr.message = "A user with ID 1 already exists, can't create admin. Try dropping tables";

            response = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(cr, response);
    }
}

