import moment from "moment";
import React from "react";
import CalendarDay from "./CalendarDay";

const Calendar = ({month: {month, startDay, endDay}, requests, ineligible}) => {

  let dayIndex = startDay.clone();

  let days = [];              // CalendarDay elements
  let elements = [];          // All elements
  let showIneligible = false;   // An ineligible date

  // Requests relevant for the day
  let currentRequestList = []; 

  // React requires repeating elements to have
  // unique keys
  let dayKey = 0;
  let weekKey = 0;

  // Keep adding elements until we're at the end
  // of the calendar page
  while (dayIndex.isBefore(endDay)) {
    // Loop for each week
    for (let i = 0; i < 7; i++) {
      // Check if there is a relevant request for
      // this particular day
      for (let j = 0; j < requests.length; j++) {
        if (
          moment(requests[j].period_start).isSameOrBefore(dayIndex) &&
          moment(requests[j].period_end).isSameOrAfter(dayIndex)
        ) {
          currentRequestList.push(requests[j]);
        }
      }

      // Check if there is this day is ineligible
      for (let j = 0; j < ineligible.length; j++) {
        const ineligibleDate = ineligible[j];
        if (
          moment(ineligibleDate.period_start).isSameOrBefore(dayIndex) &&
          moment(ineligibleDate.period_end).isSameOrAfter(dayIndex)
        ) {
          showIneligible = true;
          break;
        }
      }

      // Add the day to the day list
      days.push(
        <CalendarDay
          key={dayKey}
          day={dayIndex.date()}
          otherMonth={dayIndex.month() !== month.month()}
          requests={currentRequestList.length > 0 ? currentRequestList : false}
          ineligible={showIneligible}
        />
      );

      // Increment day index and key
      dayIndex.add(1, "day");
      dayKey++;

      // Empty out the request list and ineligible date 
      currentRequestList = [];
      showIneligible = false;
    }

    // For every week, add a row of seven days
    elements.push(<tr key={weekKey}>{days}</tr>);
    // Empty out the day elements and increment week key
    days = [];
    weekKey++;
  }

  // Finally, return the elements
  return elements;
};

export default Calendar;