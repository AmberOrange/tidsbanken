import moment from "moment";

export const isDateEligible = (date, ineligiblePeriods) => {
  for (let j = 0; j < ineligiblePeriods.length; j++) {
    const ineligibleDate = ineligiblePeriods[j];
    if (
      moment(ineligibleDate.period_start).isSameOrBefore(date) &&
      moment(ineligibleDate.period_end).isSameOrAfter(date)
    ) {
      return false;
    }
  }
  return true;
};

export const isDateRangeEligible = (startDate, endDate, ineligiblePeriods) => {
  for (
    let iDate = new Date(startDate);
    iDate <= endDate;
    iDate.setDate(iDate.getDate() + 1)
  ) {
    if (!isDateEligible(iDate, ineligiblePeriods)) {
      return false;
    }
  }
  return true;
};
